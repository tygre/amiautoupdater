/*
	AmiAutoUpdater
	Automatically update your programs!



	Copyright 2017-2018 Tygre <tygre@chingu.asia>

	This file is part of AmiAutoUpdater.

	AmiAutoUpdater is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	AmiAutoUpdater is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with AmiAutoUpdater. If not, see <http://www.gnu.org/licenses/>.



	Entirely developed on an Amiga!
	(Except for source code versioning...)
	tygre@chingu.asia
*/



/* Includes */

#include <proto/dos.h> // For RETURN_OK...
#include <stdarg.h>

#include "fortify.h"
#include "utils.h"



/* Declarations and constants */

int  init_log(void);
void close_log(void);
void log_debug(IN BOOL, IN BOOL, IN char *, ...);
void log_error(IN BOOL, IN char *, ...);
void log_normal(IN BOOL, IN char *, ...);



/* Definitions */

static BPTR output_file = 0;

int init_log(
	void)
{
	if ((output_file = Open("CON:0/0/640/200/AmiAutoUpdater", MODE_NEWFILE)) == 0)
	{
		return RETURN_ERROR;
	}
}

void close_log(
	void)
{
	Delay(500L);
	Close(output_file);
}

void log_debug(
	IN BOOL  should_print,
	IN BOOL  is_continous,
	IN char *text,
	...)
{
	va_list args;
	va_start(args, text);

	if(should_print)
	{
		FPrintf(output_file, "DEBUG: ");
		VFPrintf(output_file, text, args);
		FPrintf(output_file, "\n");
	}

	va_end(args);
}

void log_error(
	IN BOOL  is_continous,
	IN char *text,
	...)
{
	va_list args;
	va_start(args, text);

	FPrintf(output_file, "ERROR: ");
	VFPrintf(output_file, text, args);
	FPrintf(output_file, "\n");

	va_end(args);
}

void log_normal(
	IN BOOL  is_continous,
	IN char *text,
	...)
{
	va_list args;
	va_start(args, text);

	VFPrintf(output_file, text, args);
	FPrintf(output_file, "\n");

	va_end(args);
}

