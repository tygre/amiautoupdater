/*
	AmiAutoUpdater
	Automatically update your programs!



	Copyright 2017-2018 Tygre <tygre@chingu.asia>

	This file is part of AmiAutoUpdater.

	AmiAutoUpdater is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	AmiAutoUpdater is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with AmiModRadio. If not, see <http://www.gnu.org/licenses/>.



	Entirely developed on an Amiga!
	(Except for source code versioning...)
	tygre@chingu.asia
*/



#ifndef LOG_H
#define LOG_H 1

#include <exec/types.h> // For BOOL

#include "utils.h"      // For IN, OUT...

int  init_log(void);

void close_log(void);

void log_debug(
	IN BOOL  should_print,
	IN BOOL  is_continous,
	IN char *text,
	...);

void log_error(
	IN BOOL  is_continous,
	IN char *text,
	...);

void log_normal(
	IN BOOL  is_continous,
	IN char *text,
	...);

#endif
