/*
	AmiAutoUpdater
	Automatically update your programs!



	Copyright 2017-2018 Tygre <tygre@chingu.asia>

	This file is part of AmiAutoUpdater.

	AmiAutoUpdater is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	AmiAutoUpdater is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with AmiAutoUpdater. If not, see <http://www.gnu.org/licenses/>.



	Entirely developed on an Amiga!
	(Except for source code versioning...)
	tygre@chingu.asia
*/



/* Includes */

#include <exec/execbase.h>
#include <workbench/startup.h>

#include <proto/exec.h>
#include <proto/dos.h>
#include <proto/icon.h>

#include <string.h>
#include <stdio.h>

#include "fortify.h"
#include "http.h"
#include "log.h"
#include "utils.h"
#include "version.h"



/* Declarations and constants */

#define DEBUG           1
#define FTP_HEADER      "ftp://"
#define HTTP_HEADER     "http://"
#define LAST_MODIFIED   "Last-Modified: "
#define TEMP_PATH       "T:AmiAutoUpdater"
#define MAX_PATH_LENGTH 108

	   int main(int, char **);
static int _get_url_and_file(IN int, IN char **, OUT char **, OUT char **, OUT BOOL *);
static int _get_file_date_at_url(IN char *, OUT long *);
static int _get_file_date_at_file(IN char *, OUT long *);
static int _get_file_at_url(IN char *, IN char *);
static int _get_path_of_file(IN char *, OUT char **);
static int _extract_file(IN char *, IN BOOL, IN char *);
static int _get_three_letter_month_number(IN char *);



/* Definitions */

	   struct Library *IconBase      = NULL;
	   struct Library *SocketBase    = NULL;
static CONST_STRPTR    args_template = "URL/A,FILE/A,IGNOREMISSINGFILE/S";
static int             args_number   = 3;
static long            args_array[]  = { 0, 0, 0 };

int main(
	int    argc,
	char **argv)
{
	char *url                 = NULL;
	char *file                = NULL;
	BOOL  ignore_missing_file = FALSE;
	char *path                = NULL;
	long  file_date           = -1;
	long  url_date            = -1;
	
	Fortify_EnterScope();

	if(init_log() == RETURN_ERROR)
	{
		goto _RETURN_ERROR;
	}
	
	if(IconBase == NULL)
	{
		if((IconBase = OpenLibrary("icon.library", 37L)) == NULL)
		{
		   log_error(FALSE, "Error in main(int, char), could not open icon.library v32+");
			goto _RETURN_ERROR;
		}
	}

	if(SocketBase == NULL)
	{
		if((SocketBase = OpenLibrary("bsdsocket.library", 4L)) == NULL)
		{
			log_error(FALSE, "Error in main(int, char), could not open bsdsocket.library v4+");
			goto _RETURN_ERROR;
		}
	}

   if(_get_url_and_file(argc, argv, &url, &file, &ignore_missing_file) == RETURN_ERROR)
	{
		log_error(FALSE, "Could not find mandatory arguments URL or FILE");
		goto _RETURN_ERROR;
	}
	log_debug(DEBUG, FALSE, "URL=\"%s\"\nFILE=\"%s\"\nIGNOREMISSINGFILE=%d", url, file, ignore_missing_file);

	if(_get_file_date_at_url(url, &url_date) == RETURN_ERROR)
	{
		log_error(FALSE, "Could not find file at %s", url);
		goto _RETURN_ERROR;
	}
	log_debug(DEBUG, FALSE, "url_date=\"%ld\"", url_date);

	if(_get_file_date_at_file(file, &file_date) == RETURN_ERROR)
	{
		if(ignore_missing_file)
		{
			file_date = 0;
		}
		else
		{
			log_error(FALSE, "Could not find file at %s", file);
			goto _RETURN_ERROR;
		}
	}
	log_debug(DEBUG, FALSE, "file_date=\"%ld\"", file_date);

	if(url_date > file_date)
	{
		log_normal(FALSE, "File at %s is newer than file at %s\nDownloading and updating...", url, file);
	}
	else
	{
		log_normal(FALSE, "File at %s is not newer than file at %s", url, file);
		goto _RETURN_WARN;
	}

	if(_get_file_at_url(url, TEMP_PATH) == RETURN_ERROR)
	{
		log_error(FALSE, "Could not download file at %s in %s", url, TEMP_PATH);
		goto _RETURN_ERROR;
	}

	if(_get_path_of_file(file, &path) == RETURN_ERROR)
	{
		log_error(FALSE, "Could not find path of file %s", file);
		goto _RETURN_ERROR;
	}
	log_debug(DEBUG, FALSE, "file=\"%s\"", file);
	log_debug(DEBUG, FALSE, "path=\"%s\"", path);
	
	if(_extract_file(TEMP_PATH, TRUE, path) == RETURN_ERROR)
	{
		log_error(FALSE, "Could not extract file at %s in %s", TEMP_PATH, path);
		goto _RETURN_ERROR;
	}

	goto _RETURN_OK;
	_RETURN_OK:
		free(url);
		free(file);
		free(path);
		CloseLibrary(SocketBase);
		CloseLibrary(IconBase);
		close_log();
		Fortify_LeaveScope();
		Fortify_OutputStatistics();
		return RETURN_OK;

	goto _RETURN_WARN;
	_RETURN_WARN:
		free(url);
		// Tygre 2018/10/28: Too many is better than none
		// Fortify complains that I may be free'ing twice
		// the file char pointer... I don't think so...
		free(file);
		free(path);
		CloseLibrary(SocketBase);
		CloseLibrary(IconBase);
		close_log();
		Fortify_LeaveScope();
		Fortify_OutputStatistics();
		return RETURN_WARN;

	goto _RETURN_ERROR;
	_RETURN_ERROR:
		if(url != NULL)
		{
			free(url);
		}
		if(file != NULL)
		{
			free(file);
		}
		if(path != NULL)
		{
			free(path);
		}
		if(SocketBase != NULL)
		{
			CloseLibrary(SocketBase);
		}
		if(IconBase != NULL)
		{
			CloseLibrary(IconBase);
		}
		close_log();
		Fortify_LeaveScope();
		Fortify_OutputStatistics();
		return RETURN_ERROR;
}

static int _get_url_and_file(
	IN  int    argc,
	IN  char **argv,
	OUT char **url,
	OUT char **file,
	OUT BOOL  *ignore_missing_file)
{
	// Variables when in CLI
	int                arg         = 0;
	struct RDArgs     *rdargs      = NULL;
	char              *temp_url    = NULL;
	char              *temp_file   = NULL;

	// Variables when in WB
	struct WBStartup  *wbs         = (struct WBStartup *)argv;
	struct WBArg      *wb_arg      = NULL;
	STRPTR             tooltype    = NULL;
	struct DiskObject *disk_object = NULL;

	BPTR               dir_lock    = 0;
	char              *dir_name    = NULL;

	if(argc > 0)
	{
		log_debug(DEBUG, FALSE, "Running from CLI");

		rdargs = ReadArgs(args_template, args_array, NULL);
		if(rdargs)
		{

			temp_url  = (char *)args_array[0];
			*url      = (char *)malloc((strlen(temp_url) + 1) * sizeof(char));
			strlcpy(*url, temp_url, strlen(temp_url) + 1);

			temp_file = (char *)args_array[1];
			*file     = (char *)malloc((strlen(temp_file) + 1) * sizeof(char));
			strlcpy(*file, temp_file, strlen(temp_file) + 1);

			if((long *)args_array[2] != NULL)
			{
				*ignore_missing_file = TRUE;
			}
			else
			{
				*ignore_missing_file = FALSE;
			}
			
			FreeArgs(rdargs);
		}
		else
		{
			goto _RETURN_ERROR;
		}
	}
	else
	{
		log_debug(DEBUG, FALSE, "Runing from Workbench");

		wb_arg = wbs->sm_ArgList;
		// I only care about the first argument (the program itself),
		// not about any icons that could have been selected with it.
		// 	for(arg = 0; arg < wbs->sm_NumArgs; arg++, wb_arg++)
		//	dir_lock = CurrentDir(wb_arg->wa_Lock) ;

		if(wb_arg->wa_Lock == 0)
		{
			goto _RETURN_ERROR;
		}

		disk_object = GetDiskObjectNew(wb_arg->wa_Name);
		if(disk_object)
		{
			tooltype = FindToolType(disk_object->do_ToolTypes, "URL");
			if(tooltype)
			{
				*url = (char *)malloc((strlen(tooltype) + 1) * sizeof(char));
				strlcpy(*url, tooltype, strlen(tooltype) + 1);
			}
			else
			{
				goto _RETURN_ERROR;
			}

			tooltype = FindToolType(disk_object->do_ToolTypes, "FILE");
			if(tooltype)
			{
				*file = (char *)malloc((strlen(tooltype) + 1) * sizeof(char));
				strlcpy(*file, tooltype, strlen(tooltype) + 1);
			}
			else
			{
				goto _RETURN_ERROR;
			}

			tooltype = FindToolType(disk_object->do_ToolTypes, "IGNOREMISSINGFILE");
			if(tooltype)
			{
				*ignore_missing_file = TRUE;
			}
			else
			{
				*ignore_missing_file = FALSE;
			}

			FreeDiskObject(disk_object);
			disk_object = NULL;
		}
	}

	// If the file path does not contain a volume name,
	// it is a path relative to the current directory
	// and I make sure to add this current directory to it.
	if(strchr(*file, ':') == NULL)
	{
		dir_lock = GetProgramDir();
		dir_name = (STRPTR)AllocVec(MAX_PATH_LENGTH, MEMF_PUBLIC);
		if(dir_name)
		{
			if(NameFromLock(dir_lock, dir_name, MAX_PATH_LENGTH) != 0)
			{
				log_debug(DEBUG, FALSE, "dir_name=%s", dir_name);
				if(AddPart(dir_name, *file, MAX_PATH_LENGTH))
				{
					free(*file);
					*file = dir_name;
				}
				else
				{
					goto _RETURN_ERROR;
				}
			}
		}
		else
		{
			goto _RETURN_ERROR;
		}
	}

	goto _RETURN_OK;
	_RETURN_OK:
		return RETURN_OK;

	goto _RETURN_WARN;
	_RETURN_WARN:
		return RETURN_WARN;

	goto _RETURN_ERROR;
	_RETURN_ERROR:
		if(disk_object != NULL)
		{
			FreeDiskObject(disk_object);
		}
		if(dir_name != NULL)
		{
			FreeVec(dir_name);
		}
		*ignore_missing_file = FALSE;
		return RETURN_ERROR;
}

static int _get_file_date_at_url(
	IN  char *url,
	OUT long *date)
{
	int            length                     = 0;
	char          *host_name                  = NULL;
	char          *file_name                  = NULL;
	int            socket_handle_for_commands = -1;
	http_response *response                   = NULL;
	char          *date_text                  = NULL;

	int            day, mon, yea, h, m, s;
	char           D[4], M[4];

	*date = -1;

	// FTP or HTTP?
	if(string_start_with(url, FTP_HEADER))
	{
		log_debug(DEBUG, FALSE, "Examining FTP file at %s", url);

		host_name = malloc((strlen(url) + 1) * sizeof(char));
		strcpy(host_name, (char*)url + strlen(FTP_HEADER) * sizeof(char));
		length = strchr(host_name, '/') - host_name;
		host_name[length] = '\0';
		log_debug(DEBUG, FALSE, "host_name = %s", host_name);

		length = strlen(url) - strlen(FTP_HEADER) - strlen(host_name) - 1;
		file_name = malloc((length + 1) * sizeof(char));
		strcpy(file_name, (char *)url + (strlen(FTP_HEADER) + strlen(host_name) + 1) * sizeof(char));
		file_name[length] = '\0';
		log_debug(DEBUG, FALSE, "file_name = %s", file_name);

		if(ftp_init_connection(host_name, &socket_handle_for_commands) == RETURN_ERROR)
		{
			log_error(FALSE, "Could not init FTP connection with host at %s", host_name);
			goto _RETURN_ERROR;
		}

		if(ftp_get_file_last_modified_time(socket_handle_for_commands, file_name, &date_text) == RETURN_ERROR)
		{
			log_error(FALSE, "Could not get last-modified date of %s at %s", file_name, host_name);
			ftp_close_connection(socket_handle_for_commands);
			goto _RETURN_ERROR;
		}

		log_debug(DEBUG, FALSE, "date_text=\"%s\"", date_text);
		// Tygre 2018/02/03: strptime
		// I should use strptime() but this function does not seem available...
		sscanf(date_text, "%*s %*c%*c%2d%2d%2d", &yea, &mon, &day);

		ftp_close_connection(socket_handle_for_commands);
	}
	else if(string_start_with(url, HTTP_HEADER))
	{
		log_debug(DEBUG, FALSE, "Examining HTTP file at %s", url);
		if(http_init_connection() == RETURN_ERROR)
		{
			log_error(FALSE, "Could not init HTTP connection");
			goto _RETURN_ERROR;
		}
		if(http_head(url, NULL, &response) == RETURN_ERROR)
		{
			log_error(FALSE, "Could not get answer from HEAD request");
			http_close_connection();
			goto _RETURN_ERROR;
		}

		// Tygre 2018/10/28: Not a new object...
		// I must duplicate the string starting at LAST_MODIFIED
		// because the original one will be freed with the HTTP
		// response while it is not with a FTP connection above.
		date_text = strstr(response->response_headers, LAST_MODIFIED);
		date_text = string_duplicate(date_text + strlen(LAST_MODIFIED) * sizeof(char));
		log_debug(DEBUG, FALSE, "date_text=\"%s\"", date_text);
		// Tygre 2018/02/03: strptime
		// I should use strptime() but this function does not seem available...
		sscanf(date_text, "%[a-zA-Z], %d %s %*c%*c%d %d:%d:%d", D, &day, M, &yea, &h, &m, &s);
		mon = _get_three_letter_month_number(M);

		http_free_http_response(response);
		http_close_connection();
	}
	else
	{
		log_error(FALSE, "Could not understand protocol of %s (not ftp:// or http://)", url);
		goto _RETURN_WARN;
	}

	*date  = day;
	*date += mon * 100;
	*date += yea * 10000;
	
	goto _RETURN_OK;
	_RETURN_OK:
		free(host_name);
		free(file_name);
		free(date_text);
		return RETURN_OK;

	goto _RETURN_WARN;
	_RETURN_WARN:
		free(host_name);
		free(file_name);
		free(date_text);
		return RETURN_WARN;

	goto _RETURN_ERROR;
	_RETURN_ERROR:
		if(host_name != NULL)
		{
			free(host_name);
		}
		if(file_name != NULL)
		{
			free(file_name);
		}
		if(date_text != NULL)
		{
			free(date_text);
		}
		return RETURN_ERROR;
}

static int _get_file_date_at_file(
	IN  char *path,
	OUT long *date)
{
	BPTR                  file        = 0;
	struct FileInfoBlock *fib         = NULL;
	char                  date_text[9];
	struct DateTime       dt;
	struct DateStamp      dsp;
	int day, mon, yea;
	
	*date = -1;

	if(!(file = Lock(path, SHARED_LOCK)))
	{
		log_error(FALSE, "Could not lock file at %s", path);
		goto _RETURN_ERROR;
	}

	if(!(fib = AllocDosObjectTags(DOS_FIB, TAG_END)))
	{
		log_error(FALSE, "Could not allocate DOS object");
		goto _RETURN_ERROR;
	}

	if(!Examine(file, fib))
	{
		log_error(FALSE, "Could not examine file at %s", path);
		goto _RETURN_ERROR;
	}

	dsp = fib->fib_Date;

	dt.dat_Stamp   = dsp;
	dt.dat_Format  = FORMAT_CDN;
	dt.dat_Flags   = 0;
	dt.dat_StrDay  = NULL;
	dt.dat_StrDate = date_text;
	dt.dat_StrTime = NULL;
	DateToStr(&dt);

	log_debug(DEBUG, FALSE, "date_text=\"%s\"", date_text);

	sscanf(date_text, "%d-%d-%d", &day, &mon, &yea);
	*date  = day;
	*date += mon * 100;
	*date += yea * 10000;

	goto _RETURN_OK;
	_RETURN_OK:
		FreeDosObject(DOS_FIB, fib);
		UnLock(file);
		return RETURN_OK;

	goto _RETURN_WARN;
	_RETURN_WARN:
		FreeDosObject(DOS_FIB, fib);
		UnLock(file);
		return RETURN_WARN;

	goto _RETURN_ERROR;
	_RETURN_ERROR:
		if(fib != NULL)
		{
			FreeDosObject(DOS_FIB, fib);
		}
		if(file)
		{
			UnLock(file);
		}
		return RETURN_ERROR;
}

static int _get_file_at_url(
	IN char *url,
	IN char *path)
{
	int            length                     = 0;
	char          *host_name                  = NULL;
	char          *file_name                  = NULL;
	int            socket_handle_for_commands = -1;
	char          *file_bytes                 = NULL;
	int            file_length                = 0;
	http_response *response                   = NULL;
	FILE          *file_pointer               = NULL;
	int            i                          = 0;

	// FTP or HTTP?
	if(string_start_with(url, FTP_HEADER))
	{
		log_debug(DEBUG, FALSE, "Downloading FTP file at %s", url);

		host_name = malloc((strlen(url) + 1) * sizeof(char));
		strcpy(host_name, (char*)url + strlen(FTP_HEADER) * sizeof(char));
		length = strchr(host_name, '/') - host_name;
		host_name[length] = '\0';
		log_debug(DEBUG, FALSE, "host_name = %s", host_name);

		length = strlen(url) - strlen(FTP_HEADER) - strlen(host_name) - 1;
		file_name = malloc((length + 1) * sizeof(char));
		strcpy(file_name, (char *)url + (strlen(FTP_HEADER) + strlen(host_name) + 1) * sizeof(char));
		file_name[length] = '\0';
		log_debug(DEBUG, FALSE, "file_name = %s", file_name);

		if(ftp_init_connection(host_name, &socket_handle_for_commands) == RETURN_ERROR)
		{
			log_error(FALSE, "Could not init FTP connection with host at %s", host_name);
			goto _RETURN_ERROR;
		}

		if(ftp_get_file_from_data(socket_handle_for_commands, file_name, &file_bytes, &file_length) == RETURN_ERROR)
		{
			log_error(FALSE, "Could not get file %s at %s", file_name, host_name);
			ftp_close_connection(socket_handle_for_commands);
			goto _RETURN_ERROR;
		}

		ftp_close_connection(socket_handle_for_commands);
	}
	else if(string_start_with(url, HTTP_HEADER))
	{
		log_debug(DEBUG, FALSE, "Downloading HTTP file at %s", url);
		if(http_init_connection() == RETURN_ERROR)
		{
			log_error(FALSE, "Could not init HTTP connection");
			goto _RETURN_ERROR;
		}

		if(http_get(url, NULL, &response) == RETURN_ERROR)
		{
			log_error(FALSE, "Could not get answer from GET request");
			http_close_connection();
			goto _RETURN_ERROR;
		}

		file_bytes = malloc(response->body_size_in_bytes);
		memcpy(file_bytes, response->body, response->body_size_in_bytes);
		file_length = response->body_size_in_bytes;

		http_free_http_response(response);
		http_close_connection();
	}
	else
	{
		log_error(FALSE, "Could not understand protocol of %s (not ftp:// or http://)", url);
		goto _RETURN_WARN;
	}

	// Save the bytes in a file
	file_pointer = fopen(path, "w");
	for(i = 0 ; i < file_length; i++)
	{
		putc(file_bytes[i], file_pointer);
	}
	fclose(file_pointer);
	free(file_bytes);
	file_bytes  = NULL;
	file_length = 0;

	goto _RETURN_OK;
	_RETURN_OK:
		if(host_name != NULL)
		{
			free(host_name);
		}
		if(file_name != NULL)
		{
			free(file_name);
		}
		return RETURN_OK;

	goto _RETURN_WARN;
	_RETURN_WARN:
		if(host_name != NULL)
		{
			free(host_name);
		}
		if(file_name != NULL)
		{
			free(file_name);
		}
		return RETURN_WARN;

	goto _RETURN_ERROR;
	_RETURN_ERROR:
		if(host_name != NULL)
		{
			free(host_name);
		}
		if(file_name != NULL)
		{
			free(file_name);
		}
		return RETURN_ERROR;
}

static int _get_path_of_file(
	IN  char  *file,
	OUT char **path)
{
	char *pos    = NULL;
	int   length = 0;
	
	if((pos = strrchr(file, '/')) != NULL || (pos = strrchr(file, ':')) != NULL)
	{
		length = (pos - file) + 1;
		*path  = malloc((length + 1) * sizeof(char));
		strlcpy(*path, file, length + 1);
		(*path)[length] = '\0';

		/*
		// If I know that the file is in a directory,
		// I want to go one level up to avoid a name
		// clash between the name of the directory
		// and that of the file, e.g.,
		//		Utilitaires:Musique/AmiModRadio/AmiModRadio
		if(in_directory)
		{
			// Shift away from the trailing '/' or ':'
			(*path)[length - 1] = '\0';
			
			if((pos = strrchr(*path, '/')) != NULL || (pos = strrchr(*path, ':')) != NULL)
			{
				length = (pos - (*path)) + 1;
				(*path)[length] = '\0';
			}
			else
			{
				free(path);
				*path = NULL;
			}
		}
		*/
	}
	else
	{
		*path = NULL;
	}

	goto _RETURN_OK;
	_RETURN_OK:
		return RETURN_OK;

	goto _RETURN_WARN;
	_RETURN_WARN:
		return RETURN_WARN;

	goto _RETURN_ERROR;
	_RETURN_ERROR:
		return RETURN_ERROR;
}

static int _extract_file(
	IN char *archive,
	IN BOOL  archive_files_in_drawer,
	IN char *path)
{
	char *files = NULL;
	
	if(xad_extract_archive(archive, archive_files_in_drawer, path, TRUE, &files) == RETURN_ERROR)
	{
		log_error(FALSE, "Could not extract archive %s in %s", archive, path);
		return RETURN_ERROR;
	}

	return RETURN_OK;
}

static int _get_three_letter_month_number(
	IN char *month)
{
	if (strcmp(month, "Jan") == 0)
	{
		return 1;
	}
	else if (strcmp(month, "Feb") == 0)
	{
		return 2;
	}
	else if (strcmp(month, "Mar") == 0)
	{
		return 3;
	}
	else if (strcmp(month, "Apr") == 0)
	{
		return 4;
	}
	else if (strcmp(month, "May") == 0)
	{
		return 5;
	}
	else if (strcmp(month, "Jun") == 0)
	{
		return 6;
	}
	else if (strcmp(month, "Jul") == 0)
	{
		return 7;
	}
	else if (strcmp(month, "Aug") == 0)
	{
		return 8;
	}
	else if (strcmp(month, "Sep") == 0)
	{
		return 9;
	}
	else if (strcmp(month, "Oct") == 0)
	{
		return 10;
	}
	else if (strcmp(month, "Nov") == 0)
	{
		return 11;
	}
	else if (strcmp(month, "Dec") == 0)
	{
		return 12;
	}
	else
	{
		return -1;
	}
}

