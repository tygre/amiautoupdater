/*
	AmiAutoUpdater
	Automatically update your programs!



	Copyright 2017-2018 Tygre <tygre@chingu.asia>

	This file is part of AmiAutoUpdater.

	AmiAutoUpdater is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	AmiAutoUpdater is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with AmiAutoUpdater. If not, see <http://www.gnu.org/licenses/>.



	Entirely developed on an Amiga!
	(Except for source code versioning...)
	tygre@chingu.asia
*/



/* Includes */

#include <proto/dos.h> // For RETURN_OK...
#include <stdarg.h>    // For va_start() and va_end()
#include <stdlib.h>    // For size_t
#include <string.h>    // For strchr()

#include "fortify.h"
#include "utils.h"



/* Declarations and constants */

	   int     string_count_char(IN char *, IN char);
	   char   *string_duplicate(IN char *);
	   char   *string_duplicate_n(IN char *, IN int);
	   int     string_fit(OUT char **, IN char *);
	   int     string_remove_first_char(OUT char **, IN char *, IN char);
	   int     string_snprintf(INOUT char *, IN int, IN char *, ...);
	   int     string_start_with(IN char *, IN char *);
	   size_t  strlcpy(char *, IN char *, IN size_t);
static int     _strnicmp(char const *, char const *, size_t len);
static size_t  _strnlen(IN char *, IN size_t);



/* Definitions */

int   string_count_char(
	  IN char *haystack,
	  IN char  needle)
{
	IN char *p     = haystack;
	   int   count = 0;

	do
	{
		if(*p == needle)
		{
			count++;
		}
	}
	while(*(p++));

	return count;
}

char *string_duplicate(
	  IN char *s)
{
	int   length = 0;
	char *d      = NULL;

	length = strlen(s) + 1;
	d      = malloc(length * sizeof(char));
	if(d == NULL)
	{
		return NULL;
	}
	strncpy(d, s, length);

	return d;
}

// Replacement for the string.h strndup, fixes a bug
char *string_duplicate_n(
	  IN char *str,
	  IN int   max)
{
	size_t  len = _strnlen(str, max);
	char   *res = (char *)malloc((len + 1) * sizeof(char));
	if(res != NULL)
	{
		memcpy(res, str, len);
		res[len] = '\0';
	}
	return res;
}

int   string_fit(
	  OUT char **destination,
	  IN  char  *source)
{
	int length = 0;

	if(source == NULL)
	{
		return RETURN_ERROR;
	}

	length       = strlen(source) + 1;
	*destination = malloc(length * sizeof(char));
	if(*destination == NULL)
	{
		return RETURN_ERROR;
	}
	strncpy(*destination, source, length);

	return RETURN_OK;
}

int   string_remove_first_char(
	  OUT char **bale,
	  IN  char  *haystack,
	  IN  char   needle)
{
	int   length = 0;
	char *pos    = NULL;

	if(haystack == NULL)
	{
		return RETURN_ERROR;
	}

	length = strlen(haystack) + 1;
	*bale  = malloc(length * sizeof(char));
	if(*bale == NULL)
	{
		return RETURN_ERROR;
	}
	strncpy(*bale, haystack, length);

	if((pos = strchr(*bale, needle)) != NULL)
	{
		*pos = '\0';
	}

	return RETURN_OK;
}

int   string_remove_all_chars(
	  OUT char **bale,
	  IN  char  *haystack,
	  IN  char   needle)
{
	int  size  = 0;
	int  count = 0;
	int     i  = 0;
	int     j  = 0;

	size  = strlen(haystack);
	count = string_count_char(haystack, needle);
	*bale = malloc((size - count + 1) * sizeof(char));
	if(*bale == NULL)
	{
		return RETURN_ERROR;
	}

	for(i = 0; i < size; i++)
	{
		if(haystack[i] != needle)
		{
			(*bale)[j] = haystack[i];
			j++;
		}
	}
	(*bale)[j] = '\0';

	return RETURN_OK;
}

int string_snprintf(
	INOUT char *d,
	IN    int   n,
	IN    char *format,
	...)
{
	int     count = 0;
	va_list list  = NULL;

	// Tygre 2017/10/22: snprintf
	// It seems that snprintf(...) behaves like _snprinf(...)
	//  http://stackoverflow.com/questions/7706936/is-snprintf-always-null-terminating/13067917#13067917)
	// but is also off by one: if len = count - 1, then no null character is appended!
	// So, I create my own that ensures the string is null-terminated, no matter what.

	va_start(list, format);
	count = vsnprintf(d, n, format, list);
	va_end(list);

	d[n - 1] = '\0';

	return count;
}

int   string_start_with(
	  IN char *haystack,
	  IN char *needle)
{
	size_t length_haystack = strlen(haystack);
	size_t length_needle   = strlen(needle);

	return length_haystack < length_needle ? 0 :
		   _strnicmp(haystack, needle, length_needle) == 0;
}

size_t strlcpy(
	char      *d,
	IN char   *s,
	IN size_t  l)
{
	size_t result = 0;

	if(l > 0)
	{
		result = strlen(strncpy(d, s, l - 1));
		d[l - 1] = '\0';
	}

	return result;
}

/*
** Copyright 2001, Travis Geiselbrecht. All rights reserved.
** Distributed under the terms of the NewOS License.
*/
/*
* Copyright (c) 2008 Travis Geiselbrecht
*
* Permission is hereby granted, free of charge, to any person obtaining
* a copy of this software and associated documentation files
* (the "Software"), to deal in the Software without restriction,
* including without limitation the rights to use, copy, modify, merge,
* publish, distribute, sublicense, and/or sell copies of the Software,
* and to permit persons to whom the Software is furnished to do so,
* subject to the following conditions:
*
* The above copyright notice and this permission notice shall be
* included in all copies or substantial portions of the Software.
*
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
* EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
* MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
* IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
* CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
* TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
* SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/

static int _strnicmp(
	char const *s1,
	char const *s2,
	size_t len)
{
	unsigned char c1 = '\0';
	unsigned char c2 = '\0';

	if(len > 0)
	{
		do
		{
			c1 = *s1;
			c2 = *s2;
			s1++;
			s2++;
			if(!c1)
				break;
			if(!c2)
				break;
			if(c1 == c2)
				continue;
			c1 = tolower(c1);
			c2 = tolower(c2);
			if(c1 != c2)
				break;
		}
		while (--len);
	}
	return (int)c1 - (int)c2;
}

/* Copyright (C) 2004 Free Software Foundation, Inc.

   This program is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by the
   Free Software Foundation; either version 2, or (at your option) any
   later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program; if not, write to the Free Software Foundation,
   Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.  */

/*
#ifdef HAVE_CONFIG_H
# include <config.h>
#endif

#if !defined (HAVE_STRNLEN)

#include <sys/types.h>

#if defined (HAVE_UNISTD_H)
#  include <unistd.h>
#endif

#include <stdc.h>
*/

/* Find the length of S, but scan at most MAXLEN characters.  If no '\0'
   terminator is found within the first MAXLEN characters, return MAXLEN. */
static size_t _strnlen(
	IN char   *s,
	IN size_t  maxlen)
{
	IN char *e;
	size_t   n;

	for(e = s, n = 0; *e && n < maxlen; e++, n++)
		;
	return n;
}

