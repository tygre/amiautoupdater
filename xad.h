/*
	AmiModRadio
	All of Aminet modules at your fingertips



	Copyright 2015-2017 Tygre <tygre@chingu.asia>

	This file is part of AmiModRadio.

	AmiModRadio is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	AmiModRadio is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with AmiModRadio. If not, see <http://www.gnu.org/licenses/>.



	Entirely developed on an Amiga!
	(Except for source code versioning...)
	tygre@chingu.asia
*/



#ifndef XAD_H
#define XAD_H 1

#include <exec/types.h>
#include "utils.h"

int xad_extract_archive(
	IN  char  *archive_path,
	IN  BOOL   archive_files_in_drawer,
	IN  char  *destination_path,
	IN  BOOL   overwrite,
	OUT char **list);

#endif
