/*
	AmiModRadio
	All of Aminet modules at your fingertips



	Copyright 2015-2017 Tygre <tygre@chingu.asia>

	This file is part of AmiModRadio.

	AmiModRadio is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	AmiModRadio is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with AmiModRadio. If not, see <http://www.gnu.org/licenses/>.



	Entirely developed on an Amiga!
	(Except for source code versioning...)
	tygre@chingu.asia
*/



#ifndef VERSION_H
#define VERSION_H 1

#define VERSTAG "\0$VER: AmiAutoUpdater v0.1 (2018/10/28)"
#define VSTRING "AmiAutoUpdater v0.1 (2018/10/28)\r\n"
#define VCOMMOD "AmiAutoUpdater v0.1 (2018/10/28)"

char *version_get_version(void);

#endif

