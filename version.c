/*
	AmiAutoUpdater
	Automatically update your programs!



	Copyright 2017-2018 Tygre <tygre@chingu.asia>

	This file is part of AmiAutoUpdater.

	AmiAutoUpdater is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	AmiAutoUpdater is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with AmiAutoUpdater. If not, see <http://www.gnu.org/licenses/>.



	Entirely developed on an Amiga!
	(Except for source code versioning...)
	tygre@chingu.asia
*/



/* Includes */

#include "version.h"

#include "fortify.h"

#include <proto/dos.h>



/* Constants and declarations */

char *version_get_version(void);



/* Definitions */

char *version_get_version(void)
{
	STRPTR version1 = (STRPTR)VERSTAG;
	STRPTR version2 = (STRPTR)VSTRING;
	STRPTR version3 = (STRPTR)VCOMMOD;

	return version3;
}

