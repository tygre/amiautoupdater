/*
	AmiAutoUpdater
	Automatically update your programs!



	Copyright 2017-2018 Tygre <tygre@chingu.asia>

	This file is part of AmiAutoUpdater.

	AmiAutoUpdater is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	AmiAutoUpdater is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with AmiModRadio. If not, see <http://www.gnu.org/licenses/>.



	Entirely developed on an Amiga!
	(Except for source code versioning...)
	tygre@chingu.asia
*/



#ifndef UTILS_H
#define UTILS_H 1

#include <stdlib.h> // For size_t

#define IN    const
#define OUT
#define INOUT

int   string_count_char(
	  IN    char   *haystack,
	  IN    char    needle);

char *string_duplicate(
	  IN    char   *s);

char *string_duplicate_n(
	  IN    char   *s,
	  IN    int     max);

int   string_fit(
	  OUT   char  **destination,
	  IN    char   *source);

int   string_remove_first_char(
	  OUT   char  **bale,
	  IN    char   *haystack,
	  IN    char    needle);

int   string_remove_all_chars(
	  OUT   char  **bale,
	  IN    char   *haystack,
	  IN    char    needle);

int   string_snprintf(
	  INOUT char   *d,
	  IN    int     n,
	  IN    char   *format,
	  ...);

int   string_start_with(
	  IN    char   *haystack,
	  IN    char   *needle);

size_t strlcpy(
	  char         *d,
	  IN    char   *s,
	  IN    size_t  l);

#endif
