/*
	Copyright (c) 2012-2013  Swen Kooij
	Parts copyright (c) 2015-2018 Tygre <tygre@chingu.asia>

	This file used to be part of http-client-c under the names:
		- http-client-c.h
		- stringx.h
		- urlparser.h
	This file combines the three files above and is part of AmiAutoUpdater.

	AmiAutoUpdater is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	AmiAutoUpdater is distributed in the hope fthat it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with AmiAutoUpdater. If not, see <http://www.gnu.org/licenses/>.

	Warning:
	This library does not tend to work that stable nor does it fully implent the
	standards described by IETF. For more information on the precise implentation
	of the Hyper Text Transfer Protocol: http://www.ietf.org/rfc/rfc2616.txt
*/



/* Includes */

#include "http.h"

#include "fortify.h"
#include "log.h"
#include "utils.h"

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>
#include <sys/time.h>
#include <proto/exec.h>   // For OpenLibrary() and CloseLibrary()
#include <netdb.h>        // Sockets and such
#include <errno.h>
#include <proto/socket.h> // Sockets and such
#include <sys/socket.h>   // Sockets and such
#include <sys/types.h>
#include <dos/dos.h>      // For RETURN_OK, RETURN_ERROR

#ifdef __STORM__
	struct hostent* gethostbyname(const UBYTE* name);
	LONG recv(LONG s, UBYTE* buf, LONG len, LONG flags); /* V3 */
	LONG closesocket(LONG d);
	LONG connect(LONG s, const struct sockaddr* name, LONG namelen);
	LONG send(LONG s, const UBYTE* msg, LONG len, LONG flags);
	LONG socket(LONG domain, LONG type, LONG protocol);

	#define CloseSocket closesocket

	#pragma libcall SocketBase gethostbyname D2 801
	#pragma libcall SocketBase recv 4E 218004
	#pragma libcall SocketBase closesocket 78 001
	#pragma libcall SocketBase connect 36 18003
	#pragma libcall SocketBase send 42 218004
	#pragma libcall SocketBase socket 1E 21003
	#pragma libcall SocketBase inet_addr B4 801
#endif



/* Constants and declarations */

#define DEBUG                   0
#define DEBUG_VERBOSE           0
#define MAXIMUM_HEADER_LENGTH   1024
#define TEMPORARY_BUFFER_LENGTH 16384

	   int            http_init_connection(void);
static http_response *http_options(IN char *);
	   int            http_head(IN char *, IN char *, OUT http_response **);
	   int 			  http_get(IN char *, IN char *, OUT http_response **);
	   int            http_post(IN char *, IN char *, IN char *, OUT http_response **);
	   void           http_free_http_response(IN http_response *);
static void           _http_free_http_response(
					#ifdef __STORM__
					http_response *hresp);
					#else
					IN http_response *hresp);
					#endif
	   void           http_close_connection(void);

// A prototype on the _http_head(...), _http_get(...), and http__post0(...) functions
typedef http_response *(*caller)(IN char *, IN char *, IN char *);

static http_response *_http_head(IN char *, IN char *, IN char *);
static http_response *_http_get(IN char *, IN char *, IN char *);
static http_response *_http_post(IN char *, IN char *, IN char *);
static void           _http_redirect(IN caller, IN http_response *, IN char *, IN char *);
static http_response *_http_request(IN char *, IN parsed_url *);
// static int            _http_ping_server(IN char *, IN char *);
static void           _replace_content(IN http_response *, IN http_response *);
static void           _replace_content2(
					#ifdef __STORM__
					http_response *destination,
					#else
					IN http_response *destination,
					#endif
					IN http_response *source);

static parsed_url    *_parse_url(IN char *);
static void           _free_parsed_url(IN parsed_url *);
static void           _free_parsed_url2(
					#ifdef __STORM__		
					parsed_url *purl);
					#else
					IN parsed_url *purl);
					#endif
static int            _is_scheme_char(IN int);
static char          *_hostname_to_ip(IN char *);
static char          *_inet_ntoa(struct in_addr);
static int            _inet_pton(IN int, IN char *, OUT void *);
static int            _inet_pton4(IN char *, OUT unsigned char *);
static int            _inet_pton6(IN char *, OUT unsigned char *);

static char          *_base64_encode(IN char *);
static void           _encode_block(IN unsigned char *, OUT char *, IN int);
static char          *_get_until(char *, char *);
static int            _strstr(IN char *, IN char *);
static int			  _stridx(IN char *, IN char *);
static char          *_strrpl(IN char *, IN char *, IN char *);

// Defined as NULL in controlscommon.c
extern struct Library *SocketBase;



/* Definitions */

int http_init_connection(void)
{
	if(SocketBase == NULL)
	{
		if((SocketBase = OpenLibrary("bsdsocket.library", 4)) == NULL)
		{
			log_error(FALSE, "Error in ftp_init_connection(), could not open bsdsocket.library v4");
			return RETURN_ERROR;
		}
	}
	return RETURN_OK;
}

static http_response* http_options(
	IN char *url)
{
	parsed_url    *purl              = NULL;
	char          *http_headers      = NULL;
	int            http_headers_length = 0;
	char          *upwd              = NULL;
	char          *base64            = NULL;
	char          *auth_header       = NULL;
	http_response *hresp             = NULL;

	// Parse URL
	purl = _parse_url(url);
	if(purl == NULL)
	{
		log_error(FALSE, "Error in http_options(char *), could not parse URL");
		return NULL;
	}

	// Declare header
	http_headers_length = MAXIMUM_HEADER_LENGTH;
	http_headers        = malloc(http_headers_length * sizeof(char));

	// Build query/headers
	if(purl->path != NULL)
	{
		if(purl->query != NULL)
		{
			string_snprintf(http_headers, http_headers_length, "OPTIONS /%s?%s HTTP/1.1\r\nHost:%s\r\nConnection:close\r\n", purl->path, purl->query, purl->host);
		}
		else
		{
			string_snprintf(http_headers, http_headers_length, "OPTIONS /%s HTTP/1.1\r\nHost:%s\r\nConnection:close\r\n", purl->path, purl->host);
		}
	}
	else
	{
		if(purl->query != NULL)
		{
			string_snprintf(http_headers, http_headers_length, "OPTIONS/?%s HTTP/1.1\r\nHost:%s\r\nConnection:close\r\n", purl->query, purl->host);
		}
		else
		{
			string_snprintf(http_headers, http_headers_length, "OPTIONS / HTTP/1.1\r\nHost:%s\r\nConnection:close\r\n", purl->host);
		}
	}

	// Handle authorisation if needed
	if(purl->username != NULL)
	{
		// Form username:password pair
		upwd = malloc(MAXIMUM_HEADER_LENGTH * sizeof(char));
		string_snprintf(upwd, MAXIMUM_HEADER_LENGTH, "%s:%s", purl->username, purl->password);
		upwd = realloc(upwd, strlen(upwd) + 1);

		// Encode in Base64
		base64 = _base64_encode(upwd);

		// Form header
		auth_header = malloc(MAXIMUM_HEADER_LENGTH * sizeof(char));
		string_snprintf(auth_header, MAXIMUM_HEADER_LENGTH, "Authorization: Basic %s\r\n", base64);
		auth_header = realloc(auth_header, strlen(auth_header) + 1);

		// Add to header
		http_headers_length = strlen(http_headers) + strlen(auth_header) + 2;
		http_headers      = (char *)realloc(http_headers, http_headers_length);
		string_snprintf(http_headers, http_headers_length, "%s%s", http_headers, auth_header);

		free(auth_header);
		free(base64);
		free(upwd);
	}

	// Build headers
	string_snprintf(http_headers, http_headers_length, "%s\r\n", http_headers);
	http_headers_length = strlen(http_headers) + 1;
	http_headers      = (char *)realloc(http_headers, http_headers_length);

	// Make request and return response
	hresp = _http_request(http_headers, purl);

	if(hresp == NULL)
	{
		free(http_headers);
		_free_parsed_url(purl);
	}

	// Do NOT handle redirect
	return hresp;
}

int http_head(
	IN  char           *url,
	IN  char           *custom_headers,
	OUT http_response **hresp)
{
	*hresp = _http_head(url, custom_headers, NULL);
	if(*hresp == NULL)
	{
		return RETURN_ERROR;
	}
	else
	{
		return RETURN_OK;
	}
}

static http_response* _http_head(
	IN char *url,
	IN char *custom_headers,
	IN char *post_data)
{
	parsed_url    *purl              = NULL;
	char          *http_headers      = NULL;
	int            http_headers_length = 0;
	char          *upwd              = NULL;
	char          *base64            = NULL;
	char          *auth_header       = NULL;
	http_response *hresp             = NULL;

	// Parse URL
	purl = _parse_url(url);
	if(purl == NULL)
	{
		log_error(FALSE, "Error in _http_head(char *), could not parse URL");
		return NULL;
	}

	// Declare header
	http_headers_length = MAXIMUM_HEADER_LENGTH;
	http_headers        = malloc(http_headers_length * sizeof(char));

	// Build query/headers
	if(purl->path != NULL)
	{
		if(purl->query != NULL)
		{
			string_snprintf(http_headers, http_headers_length, "HEAD /%s?%s HTTP/1.1\r\nHost:%s\r\nConnection:close\r\n", purl->path, purl->query, purl->host);
		}
		else
		{
			string_snprintf(http_headers, http_headers_length, "HEAD /%s HTTP/1.1\r\nHost:%s\r\nConnection:close\r\n", purl->path, purl->host);
		}
	}
	else
	{
		if(purl->query != NULL)
		{
			string_snprintf(http_headers, http_headers_length, "HEAD /?%s HTTP/1.1\r\nHost:%s\r\nConnection:close\r\n", purl->query, purl->host);
		}
		else
		{
			string_snprintf(http_headers, http_headers_length, "HEAD / HTTP/1.1\r\nHost:%s\r\nConnection:close\r\n", purl->host);
		}
	}

	// Handle authorisation if needed
	if(purl->username != NULL)
	{
		// Format username:password pair
		upwd = malloc(MAXIMUM_HEADER_LENGTH * sizeof(char));
		string_snprintf(upwd, MAXIMUM_HEADER_LENGTH, "%s:%s", purl->username, purl->password);
		upwd = realloc(upwd, strlen(upwd) + 1);

		// Encode in Base64
		base64 = _base64_encode(upwd);

		// Form header
		auth_header = malloc(MAXIMUM_HEADER_LENGTH * sizeof(char));
		string_snprintf(auth_header, MAXIMUM_HEADER_LENGTH, "Authorization: Basic %s\r\n", base64);
		auth_header = realloc(auth_header, strlen(auth_header) + 1);

		// Add to header
		http_headers_length = strlen(http_headers) + strlen(auth_header) + 2;
		http_headers      = (char *)realloc(http_headers, http_headers_length);
		string_snprintf(http_headers, http_headers_length, "%s%s", http_headers, auth_header);

		free(auth_header);
		free(base64);
		free(upwd);
	}

	// Add custom headers and close
	if(custom_headers != NULL)
	{
		string_snprintf(http_headers, http_headers_length, "%s%s\r\n", http_headers, custom_headers);
	}
	else
	{
		string_snprintf(http_headers, http_headers_length, "%s\r\n", http_headers);
	}
	http_headers_length = strlen(http_headers) + 1;
	http_headers      = (char *)realloc(http_headers, http_headers_length);

	// Make request and return response
	hresp = _http_request(http_headers, purl);

	if(hresp == NULL)
	{
		free(http_headers);
		_free_parsed_url(purl);
	}
	else
	{
		// Handle redirect
		_http_redirect(_http_head, hresp, custom_headers, post_data);
	}

	return hresp;
}

int http_get(
	IN  char           *url,
	IN  char           *custom_headers,
	OUT http_response **hresp)
{
	*hresp = _http_get(url, custom_headers, NULL);
	if(*hresp == NULL)
	{
		return RETURN_ERROR;
	}
	else
	{
		return RETURN_OK;
	}
}

static http_response *_http_get(
	IN char *url,
	IN char *custom_headers,
	IN char *post_data)
{
	parsed_url    *purl              = NULL;
	char          *http_headers      = NULL;
	int            http_headers_length = 0;
	char          *upwd              = NULL;
	char          *base64            = NULL;
	char          *auth_header       = NULL;
	http_response *hresp             = NULL;

	// Parse URL
	purl = _parse_url(url);
	if(purl == NULL)
	{
		log_error(FALSE, "Error in _http_get(char *), could not parse URL");
		return NULL;
	}

	// Declare variable
	http_headers_length = MAXIMUM_HEADER_LENGTH;
	http_headers        = malloc(http_headers_length * sizeof(char));

	// Build query/headers
	if(purl->path != NULL)
	{
		if(purl->query != NULL)
		{
			string_snprintf(http_headers, http_headers_length, "GET /%s?%s HTTP/1.1\r\nHost:%s\r\nConnection:close\r\n", purl->path, purl->query, purl->host);
		}
		else
		{
			string_snprintf(http_headers, http_headers_length, "GET /%s HTTP/1.1\r\nHost:%s\r\nConnection:close\r\n", purl->path, purl->host);
		}
	}
	else
	{
		if(purl->query != NULL)
		{
			string_snprintf(http_headers, http_headers_length, "GET /?%s HTTP/1.1\r\nHost:%s\r\nConnection:close\r\n", purl->query, purl->host);
		}
		else
		{
			string_snprintf(http_headers, http_headers_length, "GET / HTTP/1.1\r\nHost:%s\r\nConnection:close\r\n", purl->host);
		}
	}

	// Handle authorisation if needed
	if(purl->username != NULL)
	{
		// Format username:password pair
		upwd = malloc(MAXIMUM_HEADER_LENGTH * sizeof(char));
		string_snprintf(upwd, MAXIMUM_HEADER_LENGTH, "%s:%s", purl->username, purl->password);
		upwd = realloc(upwd, strlen(upwd) + 1);

		// Encode in Base64
		base64 = _base64_encode(upwd);

		// Form header
		auth_header = malloc(MAXIMUM_HEADER_LENGTH * sizeof(char));
		string_snprintf(auth_header, MAXIMUM_HEADER_LENGTH, "Authorization: Basic %s\r\n", base64);
		auth_header = realloc(auth_header, strlen(auth_header) + 1);

		// Add to header
		http_headers_length = strlen(http_headers) + strlen(auth_header) + 2;
		http_headers      = (char *)realloc(http_headers, http_headers_length);
		string_snprintf(http_headers, http_headers_length, "%s%s", http_headers, auth_header);

		free(auth_header);
		free(base64);
		free(upwd);
	}

	// Add custom headers and close
	if(custom_headers != NULL)
	{
		string_snprintf(http_headers, http_headers_length, "%s%s\r\n", http_headers, custom_headers);
	}
	else
	{
		string_snprintf(http_headers, http_headers_length, "%s\r\n", http_headers);
	}
	http_headers_length = strlen(http_headers) + 1;
	http_headers      = (char *)realloc(http_headers, http_headers_length);

	// Make request and return response
	hresp = _http_request(http_headers, purl);

	if(hresp == NULL)
	{
		free(http_headers);
		_free_parsed_url(purl);
	}
	else
	{
		// Handle redirect
		_http_redirect(_http_get, hresp, custom_headers, post_data);
	}

	return hresp;
}

int http_post(
	IN  char           *url,
	IN  char           *custom_headers,
	IN  char           *post_data,
	OUT http_response **hresp)
{
	*hresp = _http_post(url, custom_headers, post_data);
	if(*hresp == NULL)
	{
		return RETURN_ERROR;
	}
	else
	{
		return RETURN_OK;
	}
}

static http_response* _http_post(
	IN char *url,
	IN char *custom_headers,
	IN char *post_data)
{
	parsed_url    *purl              = NULL;
	char          *http_headers      = NULL;
	int            http_headers_length = 0;
	char          *upwd              = NULL;
	char          *base64            = NULL;
	char          *auth_header       = NULL;
	http_response *hresp             = NULL;

	// Parse URL
	purl = _parse_url(url);
	if(purl == NULL)
	{
		log_error(FALSE, "Error in _http_post(char *), could not parse URL");
		return NULL;
	}

	// Declare header
	http_headers_length = MAXIMUM_HEADER_LENGTH;
	http_headers        = malloc(http_headers_length * sizeof(char));

	// Build query/headers
	if(purl->path != NULL)
	{
		if(purl->query != NULL)
		{
			string_snprintf(http_headers, http_headers_length, "POST /%s?%s HTTP/1.1\r\nHost:%s\r\nConnection:close\r\nContent-Length:%lu\r\nContent-Type:application/x-www-form-urlencoded\r\n", purl->path, purl->query, purl->host, strlen(post_data));
		}
		else
		{
			string_snprintf(http_headers, http_headers_length, "POST /%s HTTP/1.1\r\nHost:%s\r\nConnection:close\r\nContent-Length:%lu\r\nContent-Type:application/x-www-form-urlencoded\r\n", purl->path, purl->host, strlen(post_data));
		}
	}
	else
	{
		if(purl->query != NULL)
		{
			string_snprintf(http_headers, http_headers_length, "POST /?%s HTTP/1.1\r\nHost:%s\r\nConnection:close\r\nContent-Length:%lu\r\nContent-Type:application/x-www-form-urlencoded\r\n", purl->query, purl->host, strlen(post_data));
		}
		else
		{
			string_snprintf(http_headers, http_headers_length, "POST / HTTP/1.1\r\nHost:%s\r\nConnection:close\r\nContent-Length:%lu\r\nContent-Type:application/x-www-form-urlencoded\r\n", purl->host, strlen(post_data));
		}
	}

	// Handle authorisation if needed
	if(purl->username != NULL)
	{
		// Format username:password pair
		upwd = malloc(MAXIMUM_HEADER_LENGTH * sizeof(char));
		string_snprintf(upwd, MAXIMUM_HEADER_LENGTH, "%s:%s", purl->username, purl->password);
		upwd = realloc(upwd, strlen(upwd) + 1);

		// Encode in Base64
		base64 = _base64_encode(upwd);

		// Form header
		auth_header = malloc(MAXIMUM_HEADER_LENGTH * sizeof(char));
		string_snprintf(auth_header, MAXIMUM_HEADER_LENGTH, "Authorization: Basic %s\r\n", base64);
		auth_header = realloc(auth_header, strlen(auth_header) + 1);

		// Add to header
		http_headers_length = strlen(http_headers) + strlen(auth_header) + 2;
		http_headers      = (char *)realloc(http_headers, http_headers_length);
		string_snprintf(http_headers, http_headers_length, "%s%s", http_headers, auth_header);

		free(auth_header);
		free(base64);
		free(upwd);
	}

	// Add custom headers and close
	if(custom_headers != NULL)
	{
		string_snprintf(http_headers, http_headers_length, "%s%s\r\n", http_headers, custom_headers);
		string_snprintf(http_headers, http_headers_length, "%s\r\n%s", http_headers, post_data);
	}
	else
	{
		string_snprintf(http_headers, http_headers_length, "%s\r\n%s", http_headers, post_data);
	}
	http_headers_length = strlen(http_headers) + 1;
	http_headers      = (char *)realloc(http_headers, http_headers_length);

	// Make request and return response
	hresp = _http_request(http_headers, purl);

	if(hresp == NULL)
	{
		free(http_headers);
		_free_parsed_url(purl);
	}
	else
	{
		// Handle redirect
		_http_redirect(_http_post, hresp, custom_headers, post_data);
	}
	
	return hresp;
}

void http_free_http_response(
	IN http_response *hresp)
{
	_http_free_http_response((http_response *)hresp);;
}

static void _http_free_http_response(
	#ifdef __STORM__
	http_response *hresp)
	#else
	IN http_response *hresp)
	#endif
{
	if(hresp != NULL)
	{
		if(hresp->request_uri != NULL)
		{
			_free_parsed_url(hresp->request_uri);
			hresp->request_uri = NULL;
		}
		if(hresp->body != NULL)
		{
			free((void *)hresp->body);
			hresp->body = NULL;
		}
		if(hresp->status_code != NULL)
		{
			free((void *)hresp->status_code);
			hresp->status_code = NULL;
		}
		if(hresp->status_text != NULL)
		{
			free((void *)hresp->status_text);
			hresp->status_text = NULL;
		}
		if(hresp->request_headers != NULL)
		{
			free((void *)hresp->request_headers);
			hresp->request_headers = NULL;
		}
		if(hresp->response_headers != NULL)
		{
			free((void *)hresp->response_headers);
			hresp->response_headers = NULL;
		}
		free((void *)hresp);
		hresp = NULL;
	}
}

void http_close_connection(void)
{
	if(SocketBase != NULL)
	{
		CloseLibrary(SocketBase);
		SocketBase = NULL;
	}
}

static void _http_redirect(
	IN caller         caller_ptr,
	IN http_response *hresp,
	IN char          *custom_headers,
	IN char          *post_data)
{
	char          *location0 = NULL;
	char          *token     = NULL;
	char          *location1 = NULL;
	int            length    = 0;
	char          *location2 = NULL;
	http_response *new_hresp = NULL;

	location0 = (char *)hresp->request_uri->uri;
	log_debug(DEBUG, FALSE, "hresp->request_uri       = %s", location0);
	log_debug(DEBUG, FALSE, "hresp->status_code_value = %d", hresp->status_code_value);
	if(hresp->status_code_value > 300 && hresp->status_code_value < 399)
	{
		token = strtok((char *)hresp->response_headers, "\r\n");
		while(token != NULL)
		{
			log_debug(DEBUG, FALSE, "token = %s", token);
			if(_strstr(token, "Location:"))
			{
				// Extract URL
				location1 = _strrpl("Location: ", "", token);
				// It can happen that the location is "relative"
				log_debug(DEBUG, FALSE, "New location 1 = %s", location1);

				// Tygre 2018/01/31: Infinite redirection!
				// I am not managing HTTPS so there is a risk
				// of infinite redirection with that protocol.
				if(strcmp(location0, location1) == 0)
				{
					break;
				}

				if(!strstr(location1, ":"))
				{
					length    = strlen(hresp->request_uri->scheme)
											  + 3
											  + strlen(hresp->request_uri->host)
											  + 1
											  + strlen(location1)
											  + 1;
					location2 = malloc(length * sizeof(char));
					string_snprintf(location2, length, "%s://%s/%s", hresp->request_uri->scheme, hresp->request_uri->host, location1);
					
					log_debug(DEBUG, FALSE, "New location 2 = %s", location2);
					new_hresp =	caller_ptr(location2, custom_headers, post_data);
					_replace_content(hresp, new_hresp);
					free(new_hresp);
					
					free(location2);
					free(location1);
					
					break;
				}
				else
				{
					new_hresp =	caller_ptr(location1, custom_headers, post_data);
					_replace_content(hresp, new_hresp);
					free(new_hresp);
					
					free(location1);

					break;
				}
			}
			token = strtok(NULL, "\r\n");
		}
	}
}

static http_response *_http_request(
	IN char       *http_headers,
	IN parsed_url *purl)
{
	char               *hreq                  = NULL;
	int                 hreq_size             = 0;
	http_response      *hresp                 = NULL;
	
	int                 sock                  = -1;
	int                 temp_int              = 0;
	struct sockaddr_in *remote                = NULL;
	int                 sent                  = 0;
	
	char               *response              = (char *)calloc(1, sizeof(char));
	int                 number_of_bytes       = 0;
	char               *received_data          = NULL;
	int                 size_of_received_data = 0;

	char               *temp_char             = NULL;
	char               *status_line           = NULL;

	// Parsed URL
	if(purl == NULL)
	{
		log_error(FALSE, "Error in _http_request(...), could not parse URL");
		goto _RETURN_ERROR;
	}



// _http_ping_server(purl->ip, purl->port);
// _http_ping_server("132.207.170.31", purl->port);
// _http_ping_server("133.200.160.13", purl->port);
// goto _RETURN_ERROR;



	// Create TCP socket
	if((sock = socket(AF_INET, SOCK_STREAM, IPPROTO_TCP)) < 0)
	{
		log_error(FALSE, "Error in _http_request(...), could not create socket");
		goto _RETURN_ERROR;
	}

	// Set remote->sin_addr.s_addr
	remote = (struct sockaddr_in *)malloc(sizeof(struct sockaddr_in));
	remote->sin_family = AF_INET;
  	temp_int = _inet_pton(AF_INET, purl->ip, (void *)(&(remote->sin_addr.s_addr)));
  	if(temp_int < 0)
  	{
		log_error(FALSE, "Error in _http_request(...), could not allocate s_addr");
		goto _RETURN_ERROR;
  	}
	else if(temp_int == 0)
  	{
		log_error(FALSE, "Error in _http_request(...), could not interpret IP address");
		goto _RETURN_ERROR;
  	}
	remote->sin_port = htons(atoi(purl->port));

	// Connect to server
	if(connect(sock, (struct sockaddr *)remote, sizeof(struct sockaddr)) < 0)
	{
		log_error(FALSE, "Error in _http_request(...), could not connect to server");
		goto _RETURN_ERROR;
	}

	// Send headers to server
	// Tygre 2016/05/15: Format
	// Somehow, the header sent to the Web server was broken
	// because of the missing mandatory trailing "\r\n". The
	// server was timeouting instead of sending a response.
	hreq_size = strlen(http_headers) + 2 + 1;
	hreq      = (char *)malloc((hreq_size + 1) * sizeof(char));
	string_snprintf(hreq, hreq_size + 1, "%s\r\n", http_headers);

	log_debug(DEBUG, FALSE, "request = \"%s\"", hreq);

	while(sent < hreq_size)
	{
		temp_int = send(sock, hreq + sent, hreq_size - sent, 0);
		if(temp_int == -1)
		{
			log_error(FALSE, "Error in _http_request(...), could not send headers");
			goto _RETURN_ERROR;
		}
		sent += temp_int;
	 }

	// Receive response
	received_data = calloc(sizeof(char), TEMPORARY_BUFFER_LENGTH);
	if(received_data == NULL)
	{
		log_error(FALSE, "Error in _http_request(...), could not allocate buffer");
		goto _RETURN_ERROR;
	}
	while((size_of_received_data = recv(sock, received_data, TEMPORARY_BUFFER_LENGTH - 1, 0)) > 0)
	{
		response = (char *)realloc(response, number_of_bytes + size_of_received_data);
		memcpy(response + number_of_bytes, received_data, size_of_received_data);
		number_of_bytes += size_of_received_data;
	}
	free(received_data);
	if(size_of_received_data < 0)
	{
		log_error(FALSE, "Error in _http_request(...), could not receive data from the server");
		goto _RETURN_ERROR;
	}

	// Allocate memeory for response
	/*
		const parsed_url    *request_uri;
		const unsigned char *body;
		const int            body_size_in_bytes;
		const char          *status_code;
		const int            status_code_value;
		const char          *status_text;
		const char          *request_headers;
		const char          *response_headers;
	*/
	hresp = (http_response *)malloc(sizeof(http_response));
	if(hresp == NULL)
	{
		log_error(FALSE, "Error in _http_request(...), could not allocate HTTP response");
		goto _RETURN_ERROR;
	}
	         hresp->request_uri        = purl;
	         hresp->body               = NULL;
	*(int *)&hresp->body_size_in_bytes = 0;
	         hresp->status_code        = NULL;
	*(int *)&hresp->status_code_value  = 0;
	         hresp->status_text        = NULL;
	         hresp->request_headers    = http_headers;
	         hresp->response_headers   = NULL;

	// Parse body
	log_debug(DEBUG, FALSE, "response = \"%.20s\"", response);
	temp_char       = strstr(response, "\r\n\r\n");
	number_of_bytes = number_of_bytes - (temp_char + 4 - response) * sizeof(char);
	hresp->body     = malloc(number_of_bytes);
	memcpy((void *)hresp->body, temp_char + 4, number_of_bytes);
	*(int *)&hresp->body_size_in_bytes = number_of_bytes;

	// Parse status code and text
	temp_char   = _get_until(response, "\r\n");
	status_line = _strrpl("HTTP/1.1 ", "", temp_char);
	free(temp_char);
	
	temp_char = string_duplicate_n(status_line, 4);
	hresp->status_code = _strrpl(" ", "", temp_char);
	free(temp_char);
	
	temp_char = _strrpl(hresp->status_code, "", status_line);
	hresp->status_text = _strrpl(" ", "", temp_char);
	free(temp_char);
	
	*(int *)&hresp->status_code_value = atoi(hresp->status_code);

	// Parse response headers
	hresp->response_headers	= _get_until(response, "\r\n\r\n");

	goto _RETURN_OK;
	_RETURN_OK:
		free(status_line);
		free(hreq);
		CloseSocket(sock);
		free(remote);
		// free(hresp); // I don't free the object that I am returning
		free(response);
		return hresp;

	goto _RETURN_ERROR;
	_RETURN_ERROR:
		if(status_line != NULL)
		{
			free(status_line);
		}
		if(hreq != NULL)
		{
			free(hreq);
		}
		if(sock > -1)
		{
			CloseSocket(sock);
		}
		if(remote != NULL)
		{
			free(remote);
		}
		if(hresp != NULL)
		{
			free(hresp);
		}
		if(response != NULL)
		{
			free(response);
		}
		return NULL;
}

// From http://stackoverflow.com/questions/2597608/c-socket-connection-timeout/2597774#2597774
// From http://eab.abime.net/showpost.php?p=1154590&postcount=4
/*
static int _http_ping_server(
	IN char *ip,
	IN char *port)
{
	int                 sockfd;
	struct sockaddr_in  addr;
	struct hostent     *host;
	int                 error        = 0;
	int                 ret;
	long                errlen;
	struct timeval      timeout;
	fd_set              fdrset;
	fd_set              fdwset;
	int                 c;
	char               *cptr;
	long                timeout_sec  = 0;
	long		        timeout_usec = 0;
	long                one          = 1;

	sockfd = socket (AF_INET, SOCK_STREAM, 0);

	memset(&addr, 0, sizeof(addr));

	if ((host = gethostbyname(ip)) == NULL) {
		fprintf(stderr, "error: host not found");
		return RETURN_ERROR;
	}

	memcpy(&addr.sin_addr, host->h_addr_list[0], host->h_length);
	addr.sin_family = host->h_addrtype;
	addr.sin_port = htons(atoi(port));

	// ret = fcntl(sockfd, F_GETFL, 0);
	// fcntl(sockfd,       F_SETFL, ret | O_NONBLOCK);
	// printf("IoctSocket returns %ld\n", IoctlSocket(sockfd, FIONBIO, (void *)&one));
	printf("IoctSocket returns %ld\n", IoctlSocket(sockfd, FIONBIO, "1"));

	if ((ret = connect(sockfd, (struct sockaddr *) &addr, sizeof(addr))) != 0)
	{
		if (errno != EINPROGRESS)
		{
			if (errno == ECONNREFUSED) {
				fprintf(stdout, "%s port %s closed.\n", ip, port);
				close(sockfd);
				return(1);
			}
			else
			{
				fprintf(stderr, "error: %s port %s: %d\n", ip, port, errno);
				return RETURN_ERROR;
			}
		}
		fprintf(stdout, "%s port %s THERE.\n", ip, port);

		FD_ZERO(&fdrset);
		FD_SET(sockfd, &fdrset);
		fdwset = fdrset;

		timeout_sec = 0;
		timeout_usec = 3;
		
		timeout.tv_sec=timeout_sec + timeout_usec / 1000000;
		timeout.tv_usec=timeout_usec % 1000000;

		if ((ret = select(sockfd+1, &fdrset, &fdwset, NULL, timeout.tv_sec+timeout.tv_usec > 0 ? &timeout : NULL)) == 0) {
			close(sockfd);
			fprintf(stdout, "%s port %s user timeout.\n", ip, port);
			return RETURN_ERROR;
		}
		if (FD_ISSET(sockfd, &fdrset) || FD_ISSET(sockfd, &fdwset)) {
			errlen = sizeof(error);
			if ((ret=getsockopt(sockfd, SOL_SOCKET, SO_ERROR, &error, &errlen)) != 0) {
				fprintf(stderr, "error: %s port %s: getsockopt: %d\n", ip, port, errno);
				close(sockfd);
				return RETURN_ERROR;
			}
			if (error != 0) {
				fprintf(stdout, "%s port %s closed.\n", ip, port);
				close(sockfd);
				return RETURN_ERROR;
			}
		} else {
			fprintf(stderr, "error: select: sockfd not set\n");
			return RETURN_ERROR;
		}
	}

	close(sockfd);
	fprintf(stdout, "%s port %s open.\n", ip, port);
	return RETURN_OK;



	struct sockaddr_in *remote   = NULL;
	int                 result   = 0;
	int 	     	    sock     = -1;
	fd_set              fd_read;
	fd_set              fd_write;
	fd_set              fd_excpt;
	struct timeval      tv;
	int                 so_error = 0;
	long                len      = 0;
	long                one      = 1;

	log_debug(DEBUG || TRUE, FALSE, "address = %s:%s", addr, port);

	// Create TCP socket
	if((sock = socket(AF_INET, SOCK_STREAM, IPPROTO_TCP)) < 0)
	// if((sock = socket(AF_INET, SOCK_STREAM, 0)) < 0)
	{
		log_error(FALSE, "Error in _http_ping_server(...), could not create socket");
		goto _RETURN_ERROR;
	}
	// Set non-blocking mode
	IoctlSocket(sock, FIONBIO, (void *)&one);

	// Set remote->sin_addr.s_addr
	remote = (struct sockaddr_in *)malloc(sizeof(struct sockaddr_in));
	remote->sin_family = AF_INET;
	result = _inet_pton(AF_INET, addr, (void *)(&(remote->sin_addr.s_addr)));
	if(result < 0)
  	{
		log_error(FALSE, "Error in _http_ping_server(...), could not allocate s_addr");
		goto _RETURN_ERROR;
  	}
	else if(result == 0)
  	{
		log_error(FALSE, "Error in _http_ping_server(...), could not interpret IP address");
		goto _RETURN_ERROR;
  	}
	remote->sin_port = htons(atoi(port));

	// Connect to server
	if(connect(sock, (struct sockaddr *)remote, sizeof(struct sockaddr)) < 0)
	{
		log_debug(DEBUG || TRUE, FALSE, "Log in _http_ping_server(...), could not connect to server");

		printf("HERE0 %d\n", errno);

		FD_ZERO(&fd_read);
		FD_ZERO(&fd_write);
		FD_ZERO(&fd_excpt);

		FD_SET(sock, &fd_read);
		FD_SET(sock, &fd_write);
		FD_SET(sock, &fd_excpt);

		tv.tv_sec  = 3;
		tv.tv_usec = 0;
		
		printf("HERE1 %ld\n", select(sock + 1, &fd_read, &fd_write, &fd_excpt, &tv));

		len = sizeof(so_error);
		getsockopt(sock, SOL_SOCKET, SO_ERROR, &so_error, &len);
		printf("HERE2 %d\n",  so_error);
		printf("HERE3 %ld\n", FD_ISSET(sock, &fd_read));
		printf("HERE4 %ld\n", FD_ISSET(sock, &fd_write));
		printf("HERE5 %ld\n", FD_ISSET(sock, &fd_excpt));
	}
	else
	{
		printf("CONNECTED\n");
	}

	goto _RETURN_OK;
	_RETURN_OK:
		free(remote);
		CloseSocket(sock);
		return RETURN_OK;

	goto _RETURN_ERROR;
	_RETURN_ERROR:
		if(sock > -1)
		{
			CloseSocket(sock);
		}
		if(remote != NULL)
		{
			free(remote);
		}
		return RETURN_ERROR;
}
*/

static void _replace_content(
	IN http_response *destination,
	IN http_response *source)
{
	_replace_content2((http_response *)destination, source);
}

static void _replace_content2(
	#ifdef __STORM__
	http_response *destination,
	#else
	IN http_response *destination,
	#endif
	IN http_response *source)
{
	/*
		const parsed_url    *request_uri;
		const unsigned char *body;
		const int            body_size_in_bytes;
		const char          *status_code;
		const int            status_code_value;
		const char          *status_text;
		const char          *request_headers;
		const char          *response_headers;
	*/

	if(destination->request_uri != NULL)
	{
		_free_parsed_url(destination->request_uri);
	}
	destination->request_uri = source->request_uri;
	
	if(destination->body != NULL)
	{
		free((void *)destination->body);
	}
	destination->body = source->body;

	*(int *)&destination->body_size_in_bytes = source->body_size_in_bytes;
	
	if(destination->status_code != NULL)
	{
		free((void *)destination->status_code);
	}
	destination->status_code = source->status_code;
	
	*(int *)&destination->status_code_value = source->status_code_value;

	if(destination->status_text != NULL)
	{
		free((void *)destination->status_text);
	}
	destination->status_text = source->status_text;
	
	if(destination->request_headers != NULL)
	{
		free((void *)destination->request_headers);
	}
	destination->request_headers = source->request_headers;
	
	if(destination->response_headers != NULL)
	{
		free((void *)destination->response_headers);
	}
	destination->response_headers = source->response_headers;
}

/*
 * Formerly in stringx.h
 */

// Encode a string with Base64
static char *_base64_encode(
	IN char *clrstr)
{
	char *b64dst = (char *)malloc(strlen(clrstr) + 50);
	char b64[] = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/";
	unsigned char in[3];
	int i, len = 0;
	int j = 0;

	b64dst[0] = '\0';
	while(clrstr[j])
	{
		len = 0;
		for(i=0; i<3; i++)
		{
			in[i] = (unsigned char)clrstr[j];
			if(clrstr[j])
			{
				len++; j++;
			}
			else in[i] = 0;
		}
		if(len)
		{
			_encode_block(in, b64dst, len);
		}
	}
	b64dst = (char *)realloc(b64dst, strlen(b64dst) + 1);
	return b64dst;
}

// Encode 3 8-bit binary bytes as 4 '6-bit' characters
static void _encode_block(
	IN unsigned char *in,
	OUT char *b64str,
	IN int len)
{
	char b64[] = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/";
	unsigned char out[5];
	out[0] = b64[ in[0] >> 2 ];
	out[1] = b64[ ((in[0] & 0x03) << 4) | ((in[1] & 0xf0) >> 4) ];
	out[2] = (unsigned char) (len > 1 ? b64[ ((in[1] & 0x0f) << 2) |
			 ((in[2] & 0xc0) >> 6) ] : '=');
	out[3] = (unsigned char) (len > 2 ? b64[ in[2] & 0x3f ] : '=');
	out[4] = '\0';
	strncat((char *)b64str, (char *)out, sizeof(out));
}

// Get all characters until '*until' has been found
static char *_get_until(char *haystack, char *until)
{
	int offset = _stridx(haystack, until);
	return string_duplicate_n(haystack, offset);
}

// Check if one string contains another string
static int _strstr(
	IN char *haystack,
	IN char *needle)
{
	char *pos = (char *)strstr(haystack, needle);
	if(pos)
		return 1;
	else
		return 0;
}

// Get the offset of one string in another string
static int _stridx(
	IN char *a, IN
	char *b)
{
	char *offset = (char *)strstr(a, b);
	return offset - a;
}

// Search and new_needle a string with another string , in a string
static char *_strrpl(
	IN char *needle,
	IN char *new_needle,
	IN char *haystack)
{
	char *p           = NULL;
	char *old         = NULL;
	char *result      = NULL;
	int   counter     = 0;
	int   needle_size = 0;
	
	needle_size = strlen(needle);
	for(p = strstr(haystack, needle); p != NULL; p = strstr(p + needle_size, needle))
	{
		counter++;
	}
	counter = (strlen(new_needle) - needle_size) * counter + strlen(haystack) + 1;
	result  = (char *)calloc(counter, sizeof(char));
	
	old     = (char *)haystack;
	for(p = strstr(haystack, needle); p != NULL; p = strstr(p + needle_size , needle))
	{
		strncpy(result + strlen(result), old, p - old);
		strncpy(result + strlen(result), new_needle, strlen(new_needle) + 1);
		old = p + needle_size;
	}
	strncpy(result + strlen(result), old, strlen(old) + 1);
	
	return result;
}

/*
 * Formerly in urlparser.h
 */

/*
	Parses a specified URL and returns the structure named 'parsed_url'
	Implemented according to:
	RFC 1738 - http://www.ietf.org/rfc/rfc1738.txt
	RFC 3986 -  http://www.ietf.org/rfc/rfc3986.txt
*/
static parsed_url *_parse_url(
				   IN char *url)
{
	// Define the variables
	parsed_url *purl;
	const char *tmpstr;
	const char *curstr;
	int         len;
	int         i;
	int         userpass_flag;
	int         bracket_flag;

	// Allocate the parsed URL
	/*
		char *uri;      // mandatory
		char *scheme;   // mandatory
		char *host;     // mandatory
		char *ip;       // mandatory
		char *port;     // optional
		char *path;     // optional
		char *query;    // optional
		char *fragment; // optional
		char *username; // optional
		char *password; // optional
	*/
	purl = (parsed_url *)malloc(sizeof(parsed_url));
	if(purl == NULL)
	{
		return NULL;
	}
	purl->uri      = NULL;
	purl->scheme   = NULL;
	purl->host     = NULL;
	purl->ip       = NULL;
	purl->port     = NULL;
	purl->path     = NULL;
	purl->query    = NULL;
	purl->fragment = NULL;
	purl->username = NULL;
	purl->password = NULL;
	
	curstr = url;

	/*
	 * <scheme>:<scheme-specific-part>
	 * <scheme> := [a-z\+\-\.]+
	 *             upper case = lower case for resiliency
	 */
	// Read scheme
	tmpstr = strchr(curstr, ':');
	if(tmpstr == NULL)
	{
		_free_parsed_url(purl);
		log_error(FALSE, "Error in _parse_url(char *), not a well-formed URL");
		return NULL;
	}

	// Get the scheme length
	len = tmpstr - curstr;

	// Check restrictions
	for (i = 0; i < len; i++)
	{
		if (_is_scheme_char(curstr[i]) == 0)
		{
			// Invalid format
			_free_parsed_url(purl);
			log_error(FALSE, "Error in _parse_url(char *), not a well-formed scheme");
			return NULL;
		}
	}

	// Copy the scheme to the storage
	purl->scheme = (char *)malloc((len + 1) * sizeof(char));
	if(purl->scheme == NULL)
	{
		_free_parsed_url(purl);
		log_error(FALSE, "Error in _parse_url(char *), could not allocate memory for scheme");
		return NULL;
	}

	strncpy((char *)purl->scheme, curstr, len);
	((char *)purl->scheme)[len] = '\0';

	// Make the character to lower if it is upper case
	for (i = 0; i < len; i++)
	{
		((char *)purl->scheme)[i] = tolower(purl->scheme[i]);
	}

	// Skip ':'
	tmpstr++;
	curstr = tmpstr;

	/*
	 * //<user>:<password>@<host>:<port>/<url-path>
	 * Any ":", "@" and "/" must be encoded.
	 */
	// Eat "//"
	for (i = 0; i < 2; i++)
	{
		if('/' != *curstr)
		{
			_free_parsed_url(purl);
			log_error(FALSE, "Error in _parse_url(char *), not a well-formed URL");
			return NULL;
		}
		curstr++;
	}

	// Check if the user (and password) are specified
	userpass_flag = 0;
	tmpstr = curstr;
	while ('\0' != *tmpstr)
	{
		if('@' == *tmpstr)
		{
			userpass_flag = 1;
			break;
		}
		else if('/' == *tmpstr)
		{
			userpass_flag = 0;
			break;
		}
		tmpstr++;
	}

	// User and password specification
	tmpstr = curstr;
	if(userpass_flag)
	{
		// Read username
		while ('\0' != *tmpstr && ':' != *tmpstr && '@' != *tmpstr)
		{
			tmpstr++;
		}
		len = tmpstr - curstr;
		purl->username = (char *)malloc((len + 1) * sizeof(char));
		if(purl->username == NULL)
		{
			_free_parsed_url(purl);
			log_error(FALSE, "Error in _parse_url(char *), not a well-formed username/password pair");
			return NULL;
		}
		strncpy((char *)purl->username, curstr, len);
		((char *)purl->username)[len] = '\0';

		// Proceed current pointer
		curstr = tmpstr;
		if(':' == *curstr)
		{
			/* Skip ':' */
			curstr++;

			// Read password
			tmpstr = curstr;
			while ('\0' != *tmpstr && '@' != *tmpstr)
			{
				tmpstr++;
			}
			len = tmpstr - curstr;
			purl->password = (char *)malloc((len + 1) * sizeof(char));
			if(purl->password == NULL)
			{
				_free_parsed_url(purl);
				log_error(FALSE, "Error in _parse_url(char *), could not allocate memory for username/password pair");
				return NULL;
			}
			strncpy((char *)purl->password, curstr, len);
			((char *)purl->password)[len] = '\0';
			curstr = tmpstr;
		}
		// Skip '@'
		if('@' != *curstr)
		{
			_free_parsed_url(purl);
			log_error(FALSE, "Error in _parse_url(char *), not a well-formed username@password pair");
			return NULL;
		}
		curstr++;
	}

	if('[' == *curstr)
	{
		bracket_flag = 1;
	}
	else
	{
		bracket_flag = 0;
	}

	// Proceed on by delimiters with reading host
	tmpstr = curstr;
	while ('\0' != *tmpstr) {
		if(bracket_flag && ']' == *tmpstr)
 		{
			// End of IPv6 address
			tmpstr++;
			break;
		}
		else if(!bracket_flag && (':' == *tmpstr || '/' == *tmpstr))
		{
			// Port number is specified
			break;
		}
		tmpstr++;
	}
	len = tmpstr - curstr;
	purl->host = (char *)malloc((len + 1) * sizeof(char));
	if(purl->host == NULL || len <= 0)
	{
		_free_parsed_url(purl);
		log_error(FALSE, "Error in _parse_url(char *), not a well-formed host");
		return NULL;
	}
	strncpy((char *)purl->host, curstr, len);
	((char *)purl->host)[len] = '\0';
	curstr = tmpstr;

	// Check if port number specified
	if(':' == *curstr)
	{
		curstr++;
		// Read port number
		tmpstr = curstr;
		while ('\0' != *tmpstr && '/' != *tmpstr)
		{
			tmpstr++;
		}
		len = tmpstr - curstr;
		purl->port = (char *)malloc((len + 1) * sizeof(char));
		if(purl->port == NULL)
		{
			_free_parsed_url(purl);
			log_error(FALSE, "Error in _parse_url(char *), could not allocate memory for port");
			return NULL;
		}
		strncpy((char *)purl->port, curstr, len);
		((char *)purl->port)[len] = '\0';
		curstr = tmpstr;
	}
	else
	{
		purl->port = malloc((2 + 1) * sizeof(char));
		strncpy((char *)purl->port, "80", 2 + 1);
	}

	// Get IP
	purl->ip = _hostname_to_ip(purl->host);

	// Set URI
	purl->uri = string_duplicate(url);

	// End of the string
	if('\0' == *curstr)
	{
		return purl;
	}

	// Skip '/'
	if('/' != *curstr)
	{
		_free_parsed_url(purl);
		log_error(FALSE, "Error in _parse_url(char *), not a well-formed domain name");
		return NULL;
	}
	curstr++;

	// Parse path
	tmpstr = curstr;
	while ('\0' != *tmpstr && '#' != *tmpstr  && '?' != *tmpstr)
	{
		tmpstr++;
	}
	len = tmpstr - curstr;
	purl->path = (char *)malloc((len + 1) * sizeof(char));
	if(purl->path == NULL)
	{
		_free_parsed_url(purl);
		log_error(FALSE, "Error in _parse_url(char *), could not allocate memory for path");
		return NULL;
	}
	strncpy((char *)purl->path, curstr, len);
	((char *)purl->path)[len] = '\0';
	curstr = tmpstr;

	// Check if a query is specified
	if('?' == *curstr)
	{
		// Skip '?'
		curstr++;
		
		// Read query
		tmpstr = curstr;
		while ('\0' != *tmpstr && '#' != *tmpstr)
		{
			tmpstr++;
		}
		len = tmpstr - curstr;
		purl->query = (char *)malloc((len + 1) * sizeof(char));
		if(purl->query == NULL)
		{
			_free_parsed_url(purl);
			log_error(FALSE, "Error in _parse_url(char *), could not allocate memory for query");
			return NULL;
		}
		strncpy((char *)purl->query, curstr, len);
		((char *)purl->query)[len] = '\0';
		curstr = tmpstr;
	}

	// Check if fragment is specified
	if('#' == *curstr)
	{
		// Skip '#'
		curstr++;
		
		// Read fragment
		tmpstr = curstr;
		while('\0' != *tmpstr)
		{
			tmpstr++;
		}
		len = tmpstr - curstr;
		purl->fragment = (char *)malloc((len + 1) * sizeof(char));
		if(purl->fragment == NULL)
 		{
			_free_parsed_url(purl);
			log_error(FALSE, "Error in _parse_url(char *), could not allocate memory for fragment");
			return NULL;
		}
		strncpy((char *)purl->fragment, curstr, len);
		((char *)purl->fragment)[len] = '\0';
		curstr = tmpstr;
	}
	
	return purl;
}

static void _free_parsed_url(
	IN parsed_url *purl)
{
	_free_parsed_url2((parsed_url *)purl);
}

static void _free_parsed_url2(
	#ifdef __STORM__		
	parsed_url *purl)
	#else
	IN parsed_url *purl)
	#endif
{
	/*
		char *uri;      // mandatory
		char *scheme;   // mandatory
		char *host;     // mandatory
		char *ip;       // mandatory
		char *port;     // optional
		char *path;     // optional
		char *query;    // optional
		char *fragment; // optional
		char *username; // optional
		char *password; // optional
	*/
	if(purl != NULL)
	{
		if(purl->uri != NULL)
		{
			free((void *)purl->uri);
			purl->uri = NULL;
		}
		if(purl->scheme != NULL)
		{
			free((void *)purl->scheme);
			purl->scheme = NULL;
		}
		if(purl->host != NULL)
		{
			free((void *)purl->host);
			purl->host = NULL;
		}
		if(purl->ip != NULL)
		{
			free((void *)purl->ip);
			purl->ip = NULL;
		}
		if(purl->port != NULL)
		{
			free((void *)purl->port);
			purl->port = NULL;
		}
		if(purl->path != NULL)
		{
			free((void *)purl->path);
			purl->path = NULL;
		}
		if(purl->query != NULL)
		{
			free((void *)purl->query);
			purl->query = NULL;
		}
		if(purl->fragment != NULL)
		{
			free((void *)purl->fragment);
			purl->fragment = NULL;
		}
		if(purl->username != NULL)
		{
			free((void *)purl->username);
			purl->username = NULL;
		}
		if(purl->password != NULL)
		{
			free((void *)purl->password);
			purl->password = NULL;
		}
		free((void *)purl);
	}
}

// Check whether the character is permitted in scheme string
static int _is_scheme_char(
	IN int c)
{
	return (!isalpha(c) && '+' != c && '-' != c && '.' != c) ? 0 : 1;
}

// Convert a hostname into a textual IP address
static char *_hostname_to_ip(
	IN char *hostname)
{
	struct hostent *he  = NULL;
	char           *ip1 = NULL;
	char           *ip2 = NULL;

	he  = gethostbyname(hostname);
	ip1 = _inet_ntoa(*((struct in_addr *)he->h_addr));

	ip2 = malloc((strlen(ip1) + 1) * sizeof(char));
	strncpy(ip2, ip1, strlen(ip1) + 1);

	return ip2;
} 

/*
 * Copyright (c) 1983 Regents of the University of California.
 * All rights reserved.  The Berkeley software License Agreement
 * specifies the terms and conditions for redistribution.
 */
static char *_inet_ntoa(
	struct in_addr in)
{
	static char b[18];
	char *p = (char *) &in;
	#define UC(b) (((int)b)&0xff)
	string_snprintf(b, sizeof(b), "%d.%d.%d.%d", UC(p[0]), UC(p[1]), UC(p[2]), UC(p[3]));
	return(b);
}

/* This is from the BIND 4.9.4 release, modified to compile by itself */

/* Copyright (c) 1996 by Internet Software Consortium.
 *
 * Permission to use, copy, modify, and distribute this software for any
 * purpose with or without fee is hereby granted, provided that the above
 * copyright notice and this permission notice appear in all copies.
 *
 * THE SOFTWARE IS PROVIDED "AS IS" AND INTERNET SOFTWARE CONSORTIUM DISCLAIMS
 * ALL WARRANTIES WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES
 * OF MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL INTERNET SOFTWARE
 * CONSORTIUM BE LIABLE FOR ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL
 * DAMAGES OR ANY DAMAGES WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR
 * PROFITS, WHETHER IN AN ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS
 * ACTION, ARISING OUT OF OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS
 * SOFTWARE.
 */

/*
#include "setup.h"

#ifndef HAVE_INET_PTON

#ifdef HAVE_SYS_PARAM_H
#include <sys/param.h>
#endif
#ifdef HAVE_SYS_SOCKET_H
#include <sys/socket.h>
#endif
#ifdef HAVE_NETINET_IN_H
#include <netinet/in.h>
#endif
#ifdef HAVE_ARPA_INET_H
#include <arpa/inet.h>
#endif
#include <string.h>
#include <errno.h>

#include "_inet_pton.h"
*/

#define IN6ADDRSZ       16
#define INADDRSZ         4
#define INT16SZ          2

/*
 * WARNING: Don't even consider trying to compile this on a system where
 * sizeof(int) < 4.  sizeof(int) > 4 is fine; all the world's not a VAX.
 */

/* int
 * _inet_pton(af, src, dst)
 *      convert from presentation format (which usually means ASCII printable)
 *      to network format (which is usually some kind of binary format).
 * return:
 *      1 if the address was valid for the specified address family
 *      0 if the address wasn't valid (`dst' is untouched in this case)
 *      -1 if some other error occurred (`dst' is untouched in this case, too)
 * notice:
 *      On Windows we store the error in the thread errno, not
 *      in the winsock error code. This is to avoid loosing the
 *      actual last winsock error. So use macro ERRNO to fetch the
 *      errno this funtion sets when returning (-1), not SOCKERRNO.
 * author:
 *      Paul Vixie, 1996.
 */
static int _inet_pton(
	IN  int   af,
	IN  char *src,
	OUT	void *dst)
{
	switch (af) {
	case AF_INET:
		return (_inet_pton4(src, (unsigned char *)dst));
	// Tygre 2017/04/14: StormC
	// StormC does not define this constant
	// and AmiAutoUpdater does not handle IPv6.
	/*
	case AF_INET6:
		return (_inet_pton6(src, (unsigned char *)dst));
	*/
	default:
		return -1;
	}
}

/* int
 * _inet_pton4(src, dst)
 *      like inet_aton() but without all the hexadecimal and shorthand.
 * return:
 *      1 if `src' is a valid dotted quad, else 0.
 * notice:
 *      does not touch `dst' unless it's returning 1.
 * author:
 *      Paul Vixie, 1996.
 */
static int _inet_pton4(
	IN  char          *src,
	OUT unsigned char *dst)
{
  static const char digits[] = "0123456789";
  int saw_digit, octets, ch;
  unsigned char tmp[INADDRSZ], *tp;

  saw_digit = 0;
  octets = 0;
  tp = tmp;
  *tp = 0;
  while((ch = *src++) != '\0') {
	const char *pch;

	if((pch = strchr(digits, ch)) != NULL) {
	  unsigned int val = *tp * 10 + (unsigned int)(pch - digits);

	  if(saw_digit && *tp == 0)
		return (0);
	  if(val > 255)
		return (0);
	  *tp = (unsigned char)val;
	  if(! saw_digit) {
		if(++octets > 4)
		  return (0);
		saw_digit = 1;
	  }
	}
	else if(ch == '.' && saw_digit) {
	  if(octets == 4)
		return (0);
	  *++tp = 0;
	  saw_digit = 0;
	}
	else
	  return (0);
  }
  if(octets < 4)
	return (0);
  memcpy(dst, tmp, INADDRSZ);
  return (1);
}

/* int
 * _inet_pton6(src, dst)
 *      convert presentation level address to network order binary form.
 * return:
 *      1 if `src' is a valid [RFC1884 2.2] address, else 0.
 * notice:
 *      (1) does not touch `dst' unless it's returning 1.
 *      (2) :: in a full address is silently ignored.
 * credit:
 *      inspired by Mark Andrews.
 * author:
 *      Paul Vixie, 1996.
 */
static int _inet_pton6(
	IN  char          *src,
	OUT unsigned char *dst)
{
	static const char  xdigits_l[] = "0123456789abcdef", xdigits_u[] = "0123456789ABCDEF";
	unsigned char      tmp[IN6ADDRSZ], *tp, *endp, *colonp;
	const char        *xdigits, *curtok;
	int                ch, saw_xdigit;
	unsigned int       val;
	const char *pch;

	memset((tp = tmp), 0, IN6ADDRSZ);
	endp = tp + IN6ADDRSZ;
	colonp = NULL;

	/* Leading :: requires some special handling. */
	if(*src == ':')
		if(*++src != ':')
			return (0);
	
	curtok     = src;
	saw_xdigit = 0;
	val        = 0;
	while((ch = *src++) != '\0')
	{
		if((pch = strchr((xdigits = xdigits_l), ch)) == NULL)
			pch = strchr((xdigits = xdigits_u), ch);
	
		if(pch != NULL)
		{
			val <<= 4;
			val |= (pch - xdigits);
			if(++saw_xdigit > 4)
				return (0);
			continue;
		}
	
		if(ch == ':')
		{
			curtok = src;
			if(!saw_xdigit)
			{
				if(colonp)
					return (0);
				colonp = tp;
				continue;
			}
			if(tp + INT16SZ > endp)
				return (0);
			*tp++ = (unsigned char) (val >> 8) & 0xff;
			*tp++ = (unsigned char) val & 0xff;
			saw_xdigit = 0;
			val = 0;
			continue;
		}
		if(ch == '.' && ((tp + INADDRSZ) <= endp) && _inet_pton4(curtok, tp) > 0)
		{
			tp += INADDRSZ;
			saw_xdigit = 0;
			break; /* '\0' was seen by _inet_pton4(). */
		}
		return (0);
	}

	if(saw_xdigit)
	{
		if(tp + INT16SZ > endp)
			return (0);
		*tp++ = (unsigned char) (val >> 8) & 0xff;
		*tp++ = (unsigned char) val & 0xff;
	}

	if(colonp != NULL)
	{
		/*
		 * Since some memmove()'s erroneously fail to handle
		 * overlapping regions, we'll do the shift by hand.
		 */
		const long n = tp - colonp;
		long i;

		if(tp == endp)
			return (0);
		for (i = 1; i <= n; i++)
		{
			endp[- i] = colonp[n - i];
			colonp[n - i] = 0;
		}
		tp = endp;
	}

	if(tp != endp)
		return (0);

	memcpy(dst, tmp, IN6ADDRSZ);
	return (1);
}

