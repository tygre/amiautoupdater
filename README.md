# AmiAutoUpdater

AmiAutoUpdater is a solution to keep your programs up-to-date! It sits between your program and its icon and it makes sure that your program is up-to-date whenever your run your program.