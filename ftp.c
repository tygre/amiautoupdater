/*
	AmiAutoUpdater
	Automatically update your programs!



	Copyright 2017-2018 Tygre <tygre@chingu.asia>

	This file is part of AmiAutoUpdater.

	AmiAutoUpdater is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	AmiAutoUpdater is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with AmiAutoUpdater. If not, see <http://www.gnu.org/licenses/>.



	Entirely developed on an Amiga!
	(Except for source code versioning...)
	tygre@chingu.asia
*/



/* Includes */

#include "ftp.h"

#include "fortify.h"
#include "log.h"
#include "utils.h"

#include <stdio.h>
#include <stdlib.h>
#include <dos/dos.h>      // For RETURN_OK, RETURN_ERROR
#include <sys/types.h>    // For u_short
#include <proto/exec.h>   // For OpenLibrary() and CloseLibrary()
#include <netdb.h>        // Sockets and such
#include <proto/socket.h> // Sockets and such
#include <sys/socket.h>   // Sockets and such

#ifdef __STORM__
	struct hostent* gethostbyname(const UBYTE* name);
	LONG recv(LONG s, UBYTE* buf, LONG len, LONG flags); /* V3 */
	LONG closesocket(LONG d);
	LONG connect(LONG s, const struct sockaddr* name, LONG namelen);
	LONG send(LONG s, const UBYTE* msg, LONG len, LONG flags);
	LONG socket(LONG domain, LONG type, LONG protocol);

	#define CloseSocket closesocket

	#pragma libcall SocketBase gethostbyname D2 801
	#pragma libcall SocketBase recv 4E 218004
	#pragma libcall SocketBase closesocket 78 001
	#pragma libcall SocketBase connect 36 18003
	#pragma libcall SocketBase send 42 218004
	#pragma libcall SocketBase socket 1E 21003
	#pragma libcall SocketBase inet_addr B4 801
#endif



/* Constants and declarations */

#define DEBUG                   0
#define DEBUG_VERBOSE           0

#define TEMPORARY_BUFFER_LENGTH 16384
#define MAXIMUM_BUFFER_LENGTH   65536
#define MAXIMUM_COMMAND_LENGTH  256
#define MAXIMUM_LINE_LENGTH     256
#define MAXIMUM_NUMBER_RETRIES  10

	   int   ftp_init_connection(IN char *, OUT int *);
	   int   ftp_get_list(IN int, IN char *, OUT char **);
	   int   ftp_get_list_from_data(IN int, IN char *, OUT char **);
	   int   ftp_check_file_exists(IN int, IN char *, OUT BOOL *);
	   int   ftp_get_file_last_modified_time(IN int, IN  char  *, OUT char **);
	   int   ftp_get_file_from_data(IN int, IN char *, OUT char **, OUT int *);
	   void  ftp_close_connection(IN  int);
static int   _connect_to_server(IN u_short, OUT int *);
static int   _connect_to_server_for_commands(IN u_short, OUT int *);
static int   _connect_to_server_for_data(IN u_short, OUT int *);
static int   _get_data_port(IN int, OUT u_short *);
static int   _execute_command(IN int, IN char *);
static int   _read_lines_from_server_until_code(IN int, IN char *, OUT char **);
static int   _read_lines_from_server(IN int, OUT char **);
static int   _read_line_from_server(IN int, OUT char **);
static int   _read_bytes_from_server(IN int, OUT char **, OUT int *);
static char *_inet_ntoa(struct in_addr);

// Defined as NULL in controlscommon.c
extern struct Library *SocketBase;



/* Definitions */

static char           *_current_host_name = NULL;

int ftp_init_connection(
	IN  char    *host_name,
	OUT int     *socket_handle_for_commands)
{
	if(SocketBase == NULL)
	{
		if((SocketBase = OpenLibrary("bsdsocket.library", 4)) == NULL)
		{
			log_error(FALSE, "Error in ftp_init_connection(), could not open bsdsocket.library v4");
			goto _RETURN_ERROR;
		}
	}
	
	_current_host_name = string_duplicate(host_name);
	
	// Connect to the command port
	if(_connect_to_server_for_commands(21, socket_handle_for_commands) == RETURN_ERROR)
	{
		goto _RETURN_ERROR;
	}

	// Exit
	goto _RETURN_OK;
	_RETURN_OK:
		return RETURN_OK;

	goto _RETURN_ERROR;
	_RETURN_ERROR:
		log_error(FALSE, "Error in ftp_init_connection(...), could not connect to server");
		if(*socket_handle_for_commands >= 0)
		{
			CloseSocket(*socket_handle_for_commands);
	    }
		return RETURN_ERROR;
}

int ftp_get_list(
	IN  int    socket_handle_for_commands,
	IN  char  *directory_name,
	OUT char **list)
{
	char *command   = NULL;
	char *list_temp = NULL;
	int   retries   = 0;

	// Allocate memory
	command = malloc(MAXIMUM_COMMAND_LENGTH * sizeof(char));
	if(command == NULL)
	{
		log_error(FALSE, "Error in ftp_get_list(...), could not allocate command");
		goto _RETURN_ERROR;
	}

	// Issue the command to list the directory of interest
	string_snprintf(command, MAXIMUM_COMMAND_LENGTH, "CWD %s\n", directory_name);
	if(_execute_command(socket_handle_for_commands, command) < 0)
	{
		log_error(FALSE, "Error in ftp_get_list(...), could not execute command");
		goto _RETURN_ERROR;
	}
	if(_read_lines_from_server_until_code(socket_handle_for_commands, "250", &list_temp) == RETURN_ERROR)
	{
		log_error(FALSE, "Error in ftp_get_list(...), did no receive expected 250 return code");
		goto _RETURN_ERROR;
	}
	free(list_temp);
	list_temp = NULL;

	string_snprintf(command, MAXIMUM_COMMAND_LENGTH, "STAT -l\n");
	if(_execute_command(socket_handle_for_commands, command) < 0)
	{
		log_error(FALSE, "Error in ftp_get_list(...), could not execute command \"STAT -l\"");
	    goto _RETURN_ERROR;
	}
	// Tygre 2015/08/01: Implementation variations
	// Aminet returns one single line with the FTP return code 211 after 
	// "STAT -l" and this line contains the content of the directory.
	// Modland returns multiple lines, the first line starting with "213-"
	// and the last line starting with "213 " and every line in between
	// being one directory or file.
	// Modland actually implements the correct protocol.
	// The difference does not bring any problem because I can use for
	// both "ftp_get_list_from_data" but it should be handled somehow.
	if(_read_lines_from_server_until_code(socket_handle_for_commands, "211", &list_temp) == RETURN_ERROR &&
	   _read_lines_from_server_until_code(socket_handle_for_commands, "213", &list_temp) == RETURN_ERROR)
	{
		log_error(FALSE, "Error in ftp_get_list(...), did not receive expected 211/213 return code");
		goto _RETURN_ERROR;
	}
	// Transfer the list
	string_fit(&*list, list_temp);

	// Exit
	goto _RETURN_OK;
	_RETURN_OK:
		free(command);
		return RETURN_OK;

	goto _RETURN_ERROR;
	_RETURN_ERROR:
		if(list_temp != NULL)
		{
			free(list_temp);
		}
		if(command != NULL)
		{
			free(command);
	    }
		return RETURN_ERROR;
}

int ftp_get_list_from_data(
	IN  int    socket_handle_for_commands,
	IN  char  *directory_name,
	OUT char **list)
{
	u_short  port                   = 0;
	int      socket_handle_for_data = -1;
	char    *command                = NULL;
	char    *list_temp              = NULL;
	char    *list_temp_for_NULL     = NULL;
	char    *list_temp_clean        = NULL;
	int      list_size              = 0;
	int      retries                = 0;

	// Allocate memory
	command = malloc(MAXIMUM_COMMAND_LENGTH * sizeof(char));
	if(command == NULL)
	{
		log_error(FALSE, "Error in ftp_get_list_from_data(...), could not allocate command");
		goto _RETURN_ERROR;
	}

	// Get the data port number
	_get_data_port(socket_handle_for_commands, &port);

	// Connect to the data port
	if(_connect_to_server_for_data(port, &socket_handle_for_data) == RETURN_ERROR)
	{
		log_error(FALSE, "Error in ftp_get_list_from_data(...), could not connect to server");
		goto _RETURN_ERROR;
	}

	// Issue the commands to list the directory of interest
	string_snprintf(command, MAXIMUM_COMMAND_LENGTH, "CWD %s\n", directory_name);
	if(_execute_command(socket_handle_for_commands, command) < 0)
	{
		log_error(FALSE, "Error in ftp_get_list_from_data(...), could not execute command");
		goto _RETURN_ERROR;
	}
	if(_read_lines_from_server_until_code(socket_handle_for_commands, "250", &list_temp) == RETURN_ERROR)
	{
		log_error(FALSE, "Error in ftp_get_list_from_data(...), did not receive expected 250 return code");
		goto _RETURN_ERROR;
	}
	free(list_temp);
	list_temp = NULL;

	string_snprintf(command, MAXIMUM_COMMAND_LENGTH, "LIST -al\n");
	if(_execute_command(socket_handle_for_commands, command) < 0)
	{
		log_error(FALSE, "Error in ftp_get_list_from_data(...), could not execute command \"LIST -al\"");
	    goto _RETURN_ERROR;
	}
	if(_read_lines_from_server_until_code(socket_handle_for_commands, "150", &list_temp) == RETURN_ERROR)
	{
		log_error(FALSE, "Error in ftp_get_list_from_data(...), did not receive expected 150 return code");
		goto _RETURN_ERROR;
	}
	free(list_temp);
	list_temp = NULL;

	// Transfer the list
	// Tygre 2015/08/01: Control vs. Data
	// I forgot that, in the data connection, I must read the data
	// until there is nothing more to read, not according to the
	// control rules: that the data would be enclosed by lines of
	// the form "XXX-" and "XXX ", where XXX is some FTP return code.
	// So, I cannot do:
	//	_read_lines_from_server(socket_handle_for_data, &list_temp);
	// but should do:
	log_debug(DEBUG, FALSE, "Reading from the command port the list...");
	if(_read_bytes_from_server(socket_handle_for_data, &list_temp, &list_size) == RETURN_ERROR)
	{
		log_error(FALSE, "Error in ftp_get_list_from_data(...), did not receive expected data");
		goto _RETURN_ERROR;
	}
	log_debug(DEBUG, FALSE, "Done reading from the command port the list of size %d", list_size);

	// Tygre 2016/10/01: NULL-termination
	// I increase the list size by one to
	// terminate it with a NULL character.
	list_size++;
	list_temp_for_NULL = realloc(list_temp, list_size);
	if(list_temp_for_NULL == NULL)
	{
		log_error(FALSE, "Error in ftp_get_list_from_data(...), could not reallocate memory for the list");
		goto _RETURN_ERROR;
	}
	list_temp = list_temp_for_NULL;
	list_temp[list_size - 1] = '\0';

	string_remove_all_chars(&list_temp_clean, list_temp, '\r');
	string_fit(&*list, list_temp_clean);

	goto _RETURN_OK;
	_RETURN_OK:
		free(list_temp_clean);
		free(list_temp);
		free(command);
	    CloseSocket(socket_handle_for_data);
		return RETURN_OK;

	goto _RETURN_ERROR;
	_RETURN_ERROR:
		if(list_temp_clean != NULL)
		{
			free(list_temp_clean);
		}
		if(list_temp != NULL)
		{
			free(list_temp);
		}
		if(command != NULL)
		{
			free(command);
	    }
		if(socket_handle_for_data >= 0)
		{
			CloseSocket(socket_handle_for_data);
		}
		return RETURN_ERROR;
}

int ftp_check_file_exists(
	IN  int   socket_handle_for_commands,
	IN  char *file_name,
	OUT BOOL *file_exists)
{
	int   result             = RETURN_ERROR;
	char *last_modified_date = NULL;

	result = ftp_get_file_last_modified_time(socket_handle_for_commands, file_name, &last_modified_date);
	if(result == RETURN_OK)
	{
		*file_exists = TRUE;
		free(last_modified_date);
		return RETURN_OK;
	}
	else
	{
		*file_exists = FALSE;
		if(last_modified_date != NULL)
		{
			free(last_modified_date);
		}
		return RETURN_ERROR;
	}
}

int ftp_get_file_last_modified_time(
	IN  int    socket_handle_for_commands,
	IN  char  *file_name,
	OUT char **file_last_modified_time)
{
	u_short  port                   = 0;
	int      socket_handle_for_data = -1;
	char    *command                = NULL;
	int      retries                = 0;

	// Allocate memory
	command = malloc(MAXIMUM_COMMAND_LENGTH * sizeof(char));
	if(command == NULL)
	{
		log_error(FALSE, "Error in ftp_check_file_exists(...), could not allocate command");
		goto _RETURN_ERROR;
	}

	// Get the data port number
	_get_data_port(socket_handle_for_commands, &port);

	// Connect to the data port
	if(_connect_to_server_for_data(port, &socket_handle_for_data) == RETURN_ERROR)
	{
		log_error(FALSE, "Error in ftp_check_file_exists(...), could not connect to server");
		goto _RETURN_ERROR;
	}

	// Issue the commands to get the file last-modified time.
	string_snprintf(command, MAXIMUM_COMMAND_LENGTH, "MDTM %s\n", file_name);
	if(_execute_command(socket_handle_for_commands, command) < 0)
	{
		log_error(FALSE, "Error in ftp_check_file_exists(...), could not execute command \"MDTM\"");
	    goto _RETURN_ERROR;
	}
	if(_read_lines_from_server_until_code(socket_handle_for_commands, "213", file_last_modified_time) == RETURN_ERROR)
	{
		log_error(FALSE, "Error in ftp_check_file_exists(...), did not receive expected 213 return code");
		goto _RETURN_ERROR;
	}

	// Exit
	goto _RETURN_OK;
	_RETURN_OK:
		free(command);
	    CloseSocket(socket_handle_for_data);
		return RETURN_OK;

	goto _RETURN_ERROR;
	_RETURN_ERROR:
		if(command != NULL)
		{
			free(command);
	    }
		if(socket_handle_for_data >= 0)
		{
			CloseSocket(socket_handle_for_data);
		}
		return RETURN_ERROR;
}

int ftp_get_file_from_data(
	IN  int    socket_handle_for_commands,
	IN  char  *file_name,
	OUT char **file_bytes,
	OUT int   *file_length)
{
	u_short  port                   = 0;
	int      socket_handle_for_data = -1;
	char    *command                = NULL;
	char    *file_temp              = NULL;
	int      retries                = 0;

	// Allocate memory
	command = malloc(MAXIMUM_COMMAND_LENGTH * sizeof(char));
	if(command == NULL)
	{
		log_error(FALSE, "Error in ftp_get_file_from_data(...), could not allocate command");
		goto _RETURN_ERROR;
	}

	// Get the data port number
	_get_data_port(socket_handle_for_commands, &port);

	// Connect to the data port
	if(_connect_to_server_for_data(port, &socket_handle_for_data) == RETURN_ERROR)
	{
		log_error(FALSE, "Error in ftp_get_file_from_data(...), could not connect to server");
		goto _RETURN_ERROR;
	}

	// Issue the commands to get the file of interest
	string_snprintf(command, MAXIMUM_COMMAND_LENGTH, "TYPE I\n");
	if(_execute_command(socket_handle_for_commands, command) < 0)
	{
		log_error(FALSE, "Error in ftp_get_file_from_data(...), could not execute command \"TYPE I\"");
		goto _RETURN_ERROR;
	}
	if(_read_lines_from_server_until_code(socket_handle_for_commands, "200", &file_temp) == RETURN_ERROR)
	{
		log_error(FALSE, "Error in ftp_get_file_from_data(...), did not receive expected 200 return code");
		goto _RETURN_ERROR;
	}
	free(file_temp);
	file_temp = NULL;

	string_snprintf(command, MAXIMUM_COMMAND_LENGTH, "RETR %s\n", file_name);
	if(_execute_command(socket_handle_for_commands, command) < 0)
	{
		log_error(FALSE, "Error in ftp_get_file_from_data(...), could not execute command \"RETR\"");
	    goto _RETURN_ERROR;
	}
	if(_read_lines_from_server_until_code(socket_handle_for_commands, "150", &file_temp) == RETURN_ERROR)
	{
		log_error(FALSE, "Error in ftp_get_file_from_data(...), did not receive expected 150 return code");
		goto _RETURN_ERROR;
	}

	// Transfer the file
	if(_read_bytes_from_server(socket_handle_for_data, &*file_bytes, file_length) == RETURN_ERROR)
	{
		log_error(FALSE, "Error in ftp_get_file_from_data(...), did not receive expected data");
		goto _RETURN_ERROR;
	}

	// Exit
	goto _RETURN_OK;
	_RETURN_OK:
		free(file_temp);
		free(command);
	    CloseSocket(socket_handle_for_data);
		return RETURN_OK;

	goto _RETURN_ERROR;
	_RETURN_ERROR:
		if(file_temp != NULL)
		{
			free(file_temp);
		}
		if(command != NULL)
		{
			free(command);
	    }
		if(socket_handle_for_data >= 0)
		{
			CloseSocket(socket_handle_for_data);
		}
		return RETURN_ERROR;
}

void ftp_close_connection(IN int socket_handle_for_commands)
{
	CloseSocket(socket_handle_for_commands);
	free(_current_host_name);
	_current_host_name = NULL;

	if(SocketBase != NULL)
	{
		CloseLibrary(SocketBase);
		SocketBase = NULL;
	}
}

static int _connect_to_server_for_commands(
	IN  u_short  port,
	OUT int     *socket_handle_for_commands)
{
	struct hostent     *hPtr    = NULL;
	char               *command = NULL;
	char               *line    = NULL;
	char               *buffer  = NULL;
	char               *temp    = NULL;

	// Allocate memory
	line = malloc(MAXIMUM_LINE_LENGTH);
	if(line == NULL)
	{
		log_error(FALSE, "Error in _connect_to_server_for_commands(...), could not allocate line");
		goto _RETURN_ERROR;
	}
	line[0] = '\0';

	command = malloc(MAXIMUM_COMMAND_LENGTH * sizeof(char));
	if(command == NULL)
	{
		log_error(FALSE, "Error in _connect_to_server_for_commands(...), could not allocate command");
		goto _RETURN_ERROR;
	}

	// Connect to server
	if(_connect_to_server(port, socket_handle_for_commands) == RETURN_ERROR)
	{
		log_error(FALSE, "Error in _connect_to_server_for_commands(...), could not connect to server");
		goto _RETURN_ERROR;
	}

	// Wait for service ready
	if(recv(*socket_handle_for_commands, line, MAXIMUM_LINE_LENGTH, 0) < 0)
	{
		log_error(FALSE, "Error in _connect_to_server_for_commands(...), could not receive anything from server");
		goto _RETURN_ERROR;
	}

	// Some debug information
	if(DEBUG)
	{
		string_remove_first_char(&temp, line, '\n');
		log_debug(DEBUG, FALSE, "Server returns:    \"%s\"\n", temp);
		free(temp);
	}

	// Send login
	string_snprintf(command, MAXIMUM_COMMAND_LENGTH, "USER anonymous\n");
	if(_execute_command(*socket_handle_for_commands, command) < 0)
	{
		log_error(FALSE, "Error in _connect_to_server_for_commands(...), could not execute command \"USER\"");
	    goto _RETURN_ERROR;
	}
	_read_lines_from_server(*socket_handle_for_commands, &buffer);
	free(buffer);
	buffer = NULL;

	// Send password
	string_snprintf(command, MAXIMUM_COMMAND_LENGTH, "PASS anonymous@anonymous.net\n");
	if(_execute_command(*socket_handle_for_commands, command) < 0)
	{
		log_error(FALSE, "Error in _connect_to_server_for_commands(...), could not execute command \"PASS\"");
	    goto _RETURN_ERROR;
	}
	_read_lines_from_server(*socket_handle_for_commands, &buffer);
	free(buffer);
	buffer = NULL;

	// Send commands
	string_snprintf(command, MAXIMUM_COMMAND_LENGTH, "SYST\n");
	if(_execute_command(*socket_handle_for_commands, command) < 0)
	{
		log_error(FALSE, "Error in _connect_to_server_for_commands(...), could not execute command \"SYST\"");
	    goto _RETURN_ERROR;
	}
	_read_lines_from_server(*socket_handle_for_commands, &buffer);
	free(buffer);
	buffer = NULL;

	string_snprintf(command, MAXIMUM_COMMAND_LENGTH, "PWD\n");
	if(_execute_command(*socket_handle_for_commands, command) < 0)
	{
		log_error(FALSE, "Error in _connect_to_server_for_commands(...), could not execute command \"PWD\"");
	    goto _RETURN_ERROR;
	}
	// _read_lines_from_server(*socket_handle_for_commands, &buffer);
	if(_read_lines_from_server_until_code(*socket_handle_for_commands, "257", &buffer) == RETURN_ERROR)
	{
		log_error(FALSE, "Error in _connect_to_server_for_commands(...), did not receive expected 257 return code");
		goto _RETURN_ERROR;
	}

	goto _RETURN_OK;
	_RETURN_OK:
		free(buffer);
		free(line);
		free(command);
		return RETURN_OK;

	goto _RETURN_ERROR;
	_RETURN_ERROR:
		if(buffer != NULL)
		{
			free(buffer);
		}
		if(line != NULL)
		{
			free(line);
	    }
		if(command != NULL)
		{
			free(command);
	    }
		return RETURN_ERROR;
}

static int _connect_to_server_for_data(
	IN  u_short  port,
	OUT int     *socket_handle)
{
	if(_connect_to_server(port, socket_handle) == RETURN_ERROR)
	{
		log_error(FALSE, "Error in _connect_to_server_for_data(...), could not connect to server");
		return RETURN_ERROR;
	}

	return RETURN_OK;
}

static int _get_data_port(
	IN  int      socket_handle_for_commands,
	OUT u_short *port)
{
	char    *command = NULL;
	char    *buffer  = NULL;
	char    *token   = NULL;
	int      i       = 0;
	u_short  p1      = 0;
	u_short  p2      = 0;

	// Allocate memory for the commands
	command = malloc(MAXIMUM_COMMAND_LENGTH * sizeof(char));
	if(command == NULL)
	{
		log_error(FALSE, "Error in _get_data_port(...), could not allocate command");
		goto _RETURN_ERROR;
	}

	string_snprintf(command, MAXIMUM_COMMAND_LENGTH, "PASV\n");
	if(_execute_command(socket_handle_for_commands, command) < 0)
	{
		log_error(FALSE, "Error in _get_data_port(...), could not execute command \"PASV\"");
	    goto _RETURN_ERROR;
	}
	if(_read_lines_from_server_until_code(socket_handle_for_commands, "227", &buffer) == RETURN_ERROR)
	{
		log_error(FALSE, "Error in _get_data_port(...), did not receive expected 227 return code");
	    goto _RETURN_ERROR;
	}

	// Parse the port returned by the server for data
	token = strtok(buffer, "(,).");
	while(token != NULL)
	{
		log_debug(DEBUG && DEBUG_VERBOSE, FALSE, "\ttoken = %s", token);
		i++;
		if(i == 6)
		{
			p1 = atoi(token);
		}
		if(i == 7)
		{
			p2 = atoi(token);
		}
		token = strtok(NULL, "(,).");
	}

	*port = 256 * p1 + p2;

	goto _RETURN_OK;
	_RETURN_OK:
		free(buffer);
		free(command);
		return RETURN_OK;

	goto _RETURN_ERROR;
	_RETURN_ERROR:
		if(buffer != NULL)
		{
			free(buffer);
		}
		if(command != NULL)
		{
			free(command);
	    }
		return RETURN_ERROR;
}

static int _connect_to_server(
	IN  u_short  port,
	OUT int     *socket_handle)
{
	struct sockaddr_in  addr;
	struct hostent     *hPtr = NULL;

	// Create socket
	if( (*socket_handle = socket(AF_INET, SOCK_STREAM, 0)) < 0)
	{
		log_error(FALSE, "Error in _connect_to_server(...), could not create socket");
	    goto _RETURN_ERROR;
	}

	// Zero out addr
	memset(&addr, 0, sizeof(addr));

	// Get host name
	hPtr = gethostbyname(_current_host_name);
	if(hPtr == NULL)
	{
		log_error(FALSE, "Error in _connect_to_server(...), could not obtain host IP address");
		goto _RETURN_ERROR;
	}

	// Set up addr for connect
	addr.sin_addr.s_addr = inet_addr(_inet_ntoa(*((struct in_addr *)hPtr->h_addr_list[0])));
	addr.sin_family = AF_INET;
	addr.sin_port = htons(port);
	
	log_debug(DEBUG, FALSE, "IP = %s, handle = %d, port = %d",
			_inet_ntoa(*((struct in_addr *)hPtr->h_addr)),
			*socket_handle,
			port);
	
	if(connect(*socket_handle, (struct sockaddr *)&addr, sizeof(struct sockaddr)) < 0)
	{
		log_error(FALSE, "Error in _connect_to_server(...), could not connect to server");
		goto _RETURN_ERROR;
	}

	goto _RETURN_OK;
	_RETURN_OK:
		return RETURN_OK;

	goto _RETURN_ERROR;
	_RETURN_ERROR:
		return RETURN_ERROR;
}

static int _execute_command(
	IN int   socket_handle,
	IN char *command)
{
	char *temp = NULL;

	// Some debug information
	if(DEBUG)
	{
		string_remove_first_char(&temp, command, '\n');
		log_debug(DEBUG, FALSE, "Executing command: %s", temp);
		free(temp);
	}

	// Send command
	if((send(socket_handle, command, strlen(command), 0)) < 0)
	{
		log_error(FALSE, "Error in _execute_command(...), could not send command");
		return RETURN_ERROR;
	}

	return RETURN_OK;
}

static int _read_lines_from_server_until_code(
	IN  int    socket_handle,
	IN  char  *expected_code,
	OUT char **lines)
{
	char *file_temp = NULL;
	int   retries   = 0;
	int   length    = 0;

	// Tygre 2015/06/13: Buffering...
	// It is possible that the command response buffer still
	// holds previous messages that were not read, so I just
	// read and discard them until the lines start with the
	// expected code, for example 200: "Command okay".
	do
	{
		if(file_temp != NULL)
		{
			free(file_temp);
			file_temp = NULL;
		}
		_read_lines_from_server(socket_handle, &file_temp);
		retries++;
		log_debug(DEBUG && DEBUG_VERBOSE, FALSE, "retries = %d", retries);
	}
	while(!string_start_with(file_temp, expected_code)
		  && !string_start_with(file_temp, "4") // Error like 421 (service not available)
		  && !string_start_with(file_temp, "5") // Error like 550 (no such file or directory)
		  && retries < MAXIMUM_NUMBER_RETRIES);

	if(string_start_with(file_temp, "4") ||
	   string_start_with(file_temp, "5") ||
	   retries == MAXIMUM_NUMBER_RETRIES)
	{
		log_error(FALSE, "Error in _read_lines_from_server_until_code(...), server did not respond with %s but with %.3s", expected_code, file_temp);
		goto _RETURN_ERROR;
	}

	length = strlen(file_temp) + 1;
	*lines = malloc(length * sizeof(char));
	if(*lines == NULL)
	{
		log_error(FALSE, "Error in _read_lines_from_server_until_code(...), could not allocate lines");
		goto _RETURN_ERROR;
	}
	strncpy(*lines, file_temp, length);

	goto _RETURN_OK;
	_RETURN_OK:
		free(file_temp);
		return RETURN_OK;

	goto _RETURN_ERROR;
	_RETURN_ERROR:
		if(file_temp != NULL)
		{
			free(file_temp);
		}
		return RETURN_ERROR;
}

static int _read_lines_from_server(
	IN  int    socket_handle,
	OUT char **lines)
{
	int   has_next_line           = 0;
	char *line                    = NULL;
	char *lines_for_realloc       = NULL;
	int   number_of_chars         = 0;
	int   maximum_number_of_chars = 0;
	char *temp                    = NULL;

	maximum_number_of_chars = MAXIMUM_BUFFER_LENGTH;
	*lines = malloc(maximum_number_of_chars);
	if(*lines == NULL)
	{
		log_error(FALSE, "Error in _read_lines_from_server(...), could not allocate lines");
		return RETURN_ERROR;
	}
	*lines[0] = '\0';

	do
	{
		if(line != NULL)
		{
			free(line);
			line = NULL;
		}
		if(_read_line_from_server(socket_handle, &line) == RETURN_ERROR)
		{
			// Tygre 2015/05/23: Not so bad?
			// Maybe it is not so bad if the received data is "wrong",
			// we can just stop reading and exit gracefully :-)
			break;
		}
		
		// Some debug information
		if(DEBUG)
		{
			string_remove_first_char(&temp, line, '\n');
			log_debug(DEBUG && DEBUG_VERBOSE, FALSE, "line = %s", temp);
			free(temp);
		}

		if(line[3] == '-')
		{
			has_next_line = 1;
		}
		else
		{
			has_next_line = 0;
		}
		number_of_chars += strlen(line) + 1;
		if(number_of_chars >= maximum_number_of_chars)
		{
	        maximum_number_of_chars += MAXIMUM_BUFFER_LENGTH;
			lines_for_realloc = realloc(*lines, maximum_number_of_chars);
			if(lines_for_realloc == NULL)
			{
				log_error(FALSE, "Error in _read_lines_from_server(...), could not reallocate lines");
		        goto _RETURN_ERROR;
			}
			*lines = lines_for_realloc;
		}
		strncat(*lines, line, maximum_number_of_chars - strlen(*lines) - 1);
		strncat(*lines, "\n", maximum_number_of_chars - strlen(*lines) - 1);
	}
	while(has_next_line);

	goto _RETURN_OK;
	_RETURN_OK:
		// Some debug information
		if(DEBUG)
		{
			string_remove_first_char(&temp, *lines, '\n');
			log_debug(DEBUG, FALSE, "Server replies: %.*s", 80, temp);
			free(temp);
		}
		free(line);
		return RETURN_OK;

	goto _RETURN_ERROR;
	_RETURN_ERROR:
		if(line != NULL)
		{
			free(line);
		}
		return RETURN_ERROR;
}

static int _read_line_from_server(
	IN  int    socket_handle,
	OUT char **line)
{
	char  c                     = '\0';
	int   size_of_received_data = 0;
	int   number_of_chars       = 0;
	char *line_temp             = NULL;

	// Allocate memory for the commands
	line_temp = malloc(MAXIMUM_LINE_LENGTH);
	if(line_temp == NULL)
	{
		log_error(FALSE, "Error in _read_line_from_server(...), could not allocate temporary line");
		goto _RETURN_ERROR;
	}

	while(TRUE)
	{
		size_of_received_data = recv(socket_handle, &c, sizeof(char), 0);
		if(size_of_received_data != sizeof(char))
		{
			log_debug(DEBUG, FALSE, "Received %d data!?", size_of_received_data);
			// Tygre 2015/05/23: Not so bad?
			// Maybe it is not so bad if the received data is "wrong",
			// we can just stop reading and exit gracefully :-)
			//	perror("size_of_received_data != sizeof(char)");
			goto _RETURN_ERROR;
		}

		if(c == '\r')
		{
			log_debug(DEBUG && DEBUG_VERBOSE, FALSE, "Received '\\r'");
			// Tygre 2015/05/24: Termination!
			// Some servers terminate a line with '\n'
			// while others do with "\r\n". So, in the
			// latter case, I also consume the '\n'.
			recv(socket_handle, &c, sizeof(char), 0);
			goto _RETURN_OK;
		}
		else if(c == '\n')
		{
			log_debug(DEBUG && DEBUG_VERBOSE, FALSE, "Received '\\n'");
			goto _RETURN_OK;
		}
		else
		{
			if(number_of_chars < MAXIMUM_LINE_LENGTH - 1)
			{
				line_temp[number_of_chars] = c;
				number_of_chars++;
	   	    }
			else
			{
				goto _RETURN_OK;
			}
		}
	}

	goto _RETURN_OK;
	_RETURN_OK:
		line_temp[number_of_chars] = '\0';
		string_fit(&*line, line_temp);
		free(line_temp);
		return RETURN_OK;

	goto _RETURN_ERROR;
	_RETURN_ERROR:
		if(line_temp != NULL)
		{
			line_temp[number_of_chars] = '\0';
			string_fit(&*line, line_temp);
			free(line_temp);
		}
		// Tygre 2015/05/23: Not so bad?
		// Maybe it is not so bad if the received data is "wrong",
		// we can just stop reading and exit gracefully :-)
		//	perror("Error in _read_line_from_server(...)");
		return RETURN_ERROR;
}

static int  _read_bytes_from_server(
	IN  int    socket_handle,
	OUT char **bytes,
	OUT int   *bytes_length)
{
	char *received_data           = NULL;
	int   size_of_received_data   = 0;
	int   maximum_number_of_bytes = 0;
	int   number_of_bytes         = 0;
	char *bytes_temp              = NULL;
	char *bytes_temp_for_realloc  = NULL;
	int   i                       = 0;

	// Allocate memory for the commands
	maximum_number_of_bytes = MAXIMUM_BUFFER_LENGTH;
	bytes_temp = malloc(maximum_number_of_bytes);
	if(bytes_temp == NULL)
	{
		log_error(FALSE, "Error in _read_bytes_from_server(...), could not allocate temporary bytes");
		goto _RETURN_ERROR;
	}

	received_data = calloc(sizeof(char), TEMPORARY_BUFFER_LENGTH);
	if(received_data == NULL)
	{
		log_error(FALSE, "Error in _read_bytes_from_server(...), could not allocate buffer");
		goto _RETURN_ERROR;
	}

	while(TRUE)
	{
		// Tygre 2015/12/04: Beautification
		i++;
		if((i % 2) == 0)
		{
			log_debug(DEBUG, TRUE, ".");
		}
		else
		{
			log_debug(DEBUG, TRUE, ":");
		}
		
		// Could be improved if I could know the size of the
		// file in advance from the listing of the FTP content.
		size_of_received_data = recv(socket_handle, received_data, TEMPORARY_BUFFER_LENGTH - 1, 0);
		if(size_of_received_data == -1)
		{
			log_error(FALSE, "Error in _read_bytes_from_server(...), received unexpected data");
			goto _RETURN_ERROR;
		}
		else if(size_of_received_data == 0)
		{
			// Done reading the data
			*bytes = malloc(number_of_bytes);
			if(*bytes == NULL)
			{
				log_error(FALSE, "Error in _read_bytes_from_server(...), could not allocate bytes");
				goto _RETURN_ERROR;
			}
			memcpy(*bytes, bytes_temp, number_of_bytes);
			*bytes_length = number_of_bytes;
			break;
		}
		else
		{
			if(number_of_bytes + size_of_received_data * sizeof(char) > maximum_number_of_bytes)
			{
				maximum_number_of_bytes += MAXIMUM_BUFFER_LENGTH * sizeof(char);
				bytes_temp_for_realloc = realloc(bytes_temp, maximum_number_of_bytes);
				if(bytes_temp_for_realloc == NULL)
				{
					log_error(FALSE, "Error in _read_bytes_from_server(...), could not reallocate temporary bytes");
					goto _RETURN_ERROR;
				}
				bytes_temp = bytes_temp_for_realloc;
			}
			memcpy(bytes_temp + (number_of_bytes * sizeof(char)), received_data, size_of_received_data * sizeof(char));
			number_of_bytes += size_of_received_data * sizeof(char);
		}
	}

	// Tygre 2015/12/04: Beautification
	log_debug(TRUE, FALSE, "");

	goto _RETURN_OK;
	_RETURN_OK:
		free(bytes_temp);
		free(received_data);
		return RETURN_OK;

	goto _RETURN_ERROR;
	_RETURN_ERROR:
		if(bytes_temp != NULL)
		{
			free(bytes_temp);
		}
		if(received_data != NULL)
		{
			free(received_data);
		}
		return RETURN_ERROR;
}

/*
 * Copyright (c) 1983 Regents of the University of California.
 * All rights reserved.  The Berkeley software License Agreement
 * specifies the terms and conditions for redistribution.
 *
 * Changes by Tygre 2017/04/22
 */
static char *_inet_ntoa(
	struct in_addr in)
{
	static char b[18];
	char *p = (char *)&in;
	#define UC(b) (((int)b)&0xff)
	string_snprintf(b, sizeof(b), "%d.%d.%d.%d", UC(p[0]), UC(p[1]), UC(p[2]), UC(p[3]));
	return b;
}

