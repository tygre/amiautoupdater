/*
	AmiAutoUpdater
	Automatically update your programs!



	Copyright 2017-2018 Tygre <tygre@chingu.asia>

	This file is part of AmiAutoUpdater.

	AmiAutoUpdater is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	AmiAutoUpdater is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with AmiAutoUpdater. If not, see <http://www.gnu.org/licenses/>.



	Entirely developed on an Amiga!
	(Except for source code versioning...)
	tygre@chingu.asia
*/



/* Includes */

#include "xad.h"

#include "fortify.h"
#include "log.h"
#include "utils.h"

#include <stdio.h>
#include <errno.h>
#include <string.h>
#include <proto/xadmaster.h>
#include <proto/exec.h>
#include <proto/dos.h>



/* Constants and declarations */

#define DEBUG          1
#define DEBUG_VERBOSE  0
#define MAXIMUM_LENGTH 1024

int xad_extract_archive(IN  char *, IN BOOL, IN  char *, BOOL, OUT char **);



/* Definitions */

struct xadMasterBase *xadMasterBase = NULL;

int xad_extract_archive(
	IN  char  *archive_path,
	IN  BOOL   archive_files_in_drawer,
	IN  char  *destination_path,
	IN  BOOL   overwrite,
	OUT char **list)
{
	struct xadArchiveInfo *ai                      = NULL;
	struct xadFileInfo    *fi                      = NULL;
	LONG                   e                       = 0;
	char                  *list_temp               = NULL;
	char                  *list_temp_for_realloc   = NULL;
	int                    number_of_chars         = 0;
	int                    maximum_number_of_chars = 0;
	UBYTE                 *destination_name        = NULL;
	struct TagItem         tag_item[4]             = { 0 };
	int                    result                  = RETURN_OK;
	char                  *pos                     = NULL;
	BPTR                   a                       = 0;
	int                    i                       = -1;
	UBYTE                  r                       = 0;

	maximum_number_of_chars = MAXIMUM_LENGTH;
	list_temp = calloc(maximum_number_of_chars, sizeof(char));
	if(list_temp == NULL)
	{
		log_error(FALSE, "Error in extract_archive(...), could not allocate memory");
		goto _RETURN_ERROR;
	}

	if((xadMasterBase = (struct xadMasterBase *)OpenLibrary("xadmaster.library", 11)) == NULL)
	{
		log_error(FALSE, "Error in extract_archive(...), could not open xadmaster.library");
		goto _RETURN_ERROR;
	}

	if(!(ai = (struct xadArchiveInfo *)xadAllocObjectA(XADOBJ_ARCHIVEINFO, 0)))
	{
		log_error(FALSE, "Error in extract_archive(...), could not allocate struct xadArchiveInfo");
		goto _RETURN_ERROR;
	}

	// Tygre 2015/12/25: Vargs
	// When compiling with C89 option of VBCC, I cannot
	// use xadGetInfo(...) and xadFileUnArc(...) so I
	// must their "A" versions with sets of tag items.
	// (See eab.abime.net/showthread.php?goto=newpost&t=80781)
	tag_item[0].ti_Tag  = XAD_INFILENAME;
	tag_item[0].ti_Data = (int) archive_path;
	tag_item[1].ti_Tag  = TAG_END;
	e = xadGetInfoA(ai, tag_item);
	if(e)
	{
		log_error(FALSE, "Error in extract_archive(...), could not complete the call to xadGetInfo(...)");
		goto _RETURN_ERROR;
	}

	log_debug(DEBUG, FALSE, "xc_ArchiverName = %s", ai->xai_Client->xc_ArchiverName);

	destination_name = malloc(MAXIMUM_LENGTH * sizeof(char));
	if(destination_name == NULL)
	{
		log_error(FALSE, "Error in  extract_archive(...), could allocate memory");
		goto _RETURN_ERROR;
	}

	// Everything is set, let's extract the content of the archive :-)
	// This loop ignores links. It considers files and directories.
	fi = ai->xai_FileInfo;
	while(fi)
	{
		CopyMem((void *)destination_path, destination_name, strlen(destination_path) + 1);
		if(archive_files_in_drawer)
		{
			if((pos = strchr(fi->xfi_FileName, '/')) != NULL)
			{
				AddPart(destination_name, pos + 1, MAXIMUM_LENGTH);
			}
			else
			{
				// This file is not in (at least) one drawer.
				// Therefore, it must be at the root of the archive and should be ignored.
				fi = fi->xfi_Next;
				continue;
			}
		}
		else
		{
			AddPart(destination_name, fi->xfi_FileName, MAXIMUM_LENGTH);
		}

		log_debug(DEBUG, FALSE, "\tDealing with %s", destination_name);

		if(fi->xfi_Flags & XADFIF_LINK)
		{
			log_debug(DEBUG, FALSE, "\tDealing with a ink");
			// Do nothing.
		} 
		else if(fi->xfi_Flags & XADFIF_DIRECTORY)
		{
			log_debug(DEBUG, FALSE, "\tDealing with a directory");

			while(destination_name[i] && !e)
			{
				for(; destination_name[i] && destination_name[i] != '/'; ++i)
				{
					// Do nothing
				}

				r = destination_name[i];
				destination_name[i] = 0;
				if((a = Lock(destination_name, SHARED_LOCK)))
				{
					UnLock(a);
				}
				else if((a = CreateDir(destination_name)))
				{
					UnLock(a);
				}
				else
				{
					e = 1;
				}
				destination_name[i++] = r;
			}
			
			if(e)
			{
				Printf("failed to create directory '%s'\n", fi->xfi_FileName);
			}
			else
			{
				Printf("Created directory   : %s\n", destination_name);
			}
		}
		else
		{
			log_debug(DEBUG, FALSE, "\tDealing with a file");

			tag_item[0].ti_Tag  = XAD_OUTFILENAME;
			tag_item[0].ti_Data = (int) destination_name;
			tag_item[1].ti_Tag  = XAD_ENTRYNUMBER;
			tag_item[1].ti_Data = fi->xfi_EntryNumber;
			tag_item[2].ti_Tag  = XAD_MAKEDIRECTORY;
			tag_item[2].ti_Data  = TRUE;
			if(overwrite)
			{
				tag_item[3].ti_Tag  = XAD_OVERWRITE;
				tag_item[3].ti_Data  = TRUE;
				tag_item[4].ti_Tag  = TAG_END;
			}
			else
			{
				tag_item[3].ti_Tag  = TAG_END;
			}
			e = xadFileUnArcA(ai, tag_item);
			if(e)
			{
				log_error(FALSE, "Error in extract_archive(...), could not complete the call to xadFileUnArc(...)");
				log_error(FALSE, "e = %ld for file = %s", e, destination_name);
				result = RETURN_ERROR;
			}
			else
			{
				number_of_chars += strlen(destination_name) + 2;
				if(number_of_chars >= maximum_number_of_chars)
				{
					maximum_number_of_chars += MAXIMUM_LENGTH;
					list_temp_for_realloc = realloc(list_temp, maximum_number_of_chars);
					if(list_temp_for_realloc == NULL)
					{
						log_error(FALSE, "Error in  extract_archive(...), could reallocate memory");
						goto _RETURN_ERROR;
					}
					list_temp = list_temp_for_realloc;
				}
				strncat(list_temp, destination_name, maximum_number_of_chars - strlen(list_temp) - 1);
				strncat(list_temp, "\n",             maximum_number_of_chars - strlen(list_temp) - 1);
			}
		}
		fi = fi->xfi_Next;
	}

	if(result == RETURN_ERROR)
	{
		goto _RETURN_ERROR;
	}

	goto _RETURN_OK;
	_RETURN_OK:
		xadFreeInfo(ai);
		xadFreeObjectA(ai, 0);
		CloseLibrary((struct Library *)xadMasterBase);
		if(string_fit(&*list, list_temp) == RETURN_ERROR)
		{
			log_error(FALSE, "Error in  extract_archive(...), could not trim list");
			goto _RETURN_ERROR;
		}
		free(destination_name);
		free(list_temp);
		return RETURN_OK;

	goto _RETURN_ERROR;
	_RETURN_ERROR:
		if(ai != NULL)
		{
			xadFreeInfo(ai);
			xadFreeObjectA(ai, 0);
		}
		if(xadMasterBase != NULL)
		{
			CloseLibrary((struct Library *)xadMasterBase);
		}
		*list = NULL;
		if(destination_name != NULL)
		{
			free(destination_name);
		}
		if(list_temp != NULL)
		{
			free(list_temp);
		}
		return RETURN_ERROR;
}
